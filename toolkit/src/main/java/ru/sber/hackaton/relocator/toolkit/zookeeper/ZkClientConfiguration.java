package ru.sber.hackaton.relocator.toolkit.zookeeper;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ZkClientConfiguration {

    private static final String ZOOKEEPER_CONNECT = "ZOOKEEPER_CONNECT";

    String getZookeeperConnection() {
        return System.getenv(ZOOKEEPER_CONNECT);
    }

    int getZookeeperSessionTimeoutInMinutes() {
        // TODO Задать настройкой.

        return 5;
    }

}
