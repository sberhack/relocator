package ru.sber.hackaton.relocator.toolkit.zookeeper;

/**
 * Ошибка в Zookeeper.
 */
public class ZkException extends RuntimeException {

    public ZkException() {
    }

    public ZkException(String message) {
        super(message);
    }

    public ZkException(String message, Throwable cause) {
        super(message, cause);
    }

    public ZkException(Throwable cause) {
        super(cause);
    }

}
