package ru.sber.hackaton.relocator.toolkit.kafka;

import org.apache.kafka.common.serialization.Deserializer;
import ru.sber.hackaton.relocator.toolkit.SerializationUtils;

import java.io.Serializable;

/**
 * Десериализатор сообщений на основе Java сериализации.
 */
public class SerializableDeserializer implements Deserializer<Serializable> {

    @Override
    public Serializable deserialize(String topic, byte[] data) {
        return SerializationUtils.deserialize(data);
    }

}
