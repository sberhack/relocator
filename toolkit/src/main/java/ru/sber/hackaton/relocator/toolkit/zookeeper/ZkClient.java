package ru.sber.hackaton.relocator.toolkit.zookeeper;

import org.apache.zookeeper.*;
import org.apache.zookeeper.client.ZKClientConfig;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sber.hackaton.relocator.toolkit.SerializationUtils;

import javax.annotation.PreDestroy;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Dependent
public class ZkClient {

    private final ZkClientConfiguration configuration;

    private ZooKeeper zooKeeper;

    private final ConcurrentHashMap<Object, List<NodeInfo>> nodes = new ConcurrentHashMap<>();

    private final Object sync = new Object();

    private static final Logger LOG = LoggerFactory.getLogger(ZkClient.class);

    @Inject
    public ZkClient(ZkClientConfiguration configuration) {
        this.configuration = configuration;
    }

    public void addEphemeralNode(Object owner, String path, Watcher watcher)
            throws InterruptedException {
        final ZooKeeper zk = getZookeeper();

        synchronized (sync) {
            final List<NodeInfo> nodeInfo =
                    nodes.computeIfAbsent(owner, key -> new ArrayList<>());

            createZkPath(zk, path, watcher);

            final NodeInfo eni = new NodeInfo();
            eni.path = path;
            eni.watcher = watcher;

            nodeInfo.add(eni);
        }
    }

    public void addEphemeralNodes(Object owner, List<String> paths, Watcher watcher)
            throws InterruptedException {
        final ZooKeeper zk = getZookeeper();

        synchronized (sync) {
            final List<NodeInfo> nodeInfo =
                    nodes.computeIfAbsent(owner, key -> new ArrayList<>());

            final List<String> successPaths = new ArrayList<>(paths.size());

            for (String path : paths) {
                try {
                    createZkPath(zk, path, watcher);

                    successPaths.add(path);

                    final NodeInfo eni = new NodeInfo();
                    eni.path = path;
                    eni.watcher = watcher;

                    nodeInfo.add(eni);
                } catch (Exception e) {
                    for (String sp : successPaths) {
                        try {
                            try {
                                zk.delete(sp, -1);
                            } finally {
                                if (watcher != null) {
                                    zk.removeWatches(sp, watcher, Watcher.WatcherType.Any, true);
                                }
                            }
                        } catch (Exception ex) {
                            e.addSuppressed(ex);
                        }
                    }

                    throw e;
                }
            }
        }
    }

    public void removeNode(Object owner, String path)
            throws InterruptedException {
        final ZooKeeper zk = getZookeeper();

        synchronized (sync) {
            final List<NodeInfo> nodeInfo = nodes.get(owner);

            if (nodeInfo != null) {
                final List<NodeInfo> nodeInfoToDelete = nodeInfo.stream()
                        .filter(x -> Objects.equals(x.path, path))
                        .collect(Collectors.toList());

                removeNodeInfo(zk, nodeInfoToDelete);
            }
        }
    }

    public void removeNodeOwner(Object owner)
            throws InterruptedException {
        final List<NodeInfo> nodeInfo;

        synchronized (sync) {
            nodeInfo = nodes.remove(owner);
        }

        final ZooKeeper zk;

        if (nodeInfo != null) {
            zk = getZookeeper();
        } else {
            zk = null;
        }

        if (nodeInfo != null) {
            removeNodeInfo(zk, nodeInfo);
        }
    }

    public void addWatcher(Object owner, String path, Watcher watcher)
            throws InterruptedException {
        final ZooKeeper zk = getZookeeper();

        synchronized (sync) {
            final List<NodeInfo> nodeInfo =
                    nodes.computeIfAbsent(owner, key -> new ArrayList<>());

            if (nodeInfo.stream().anyMatch(x -> Objects.equals(x.path, path))) {
                // Такой вотчер уже есть.
                return;
            }

            try {
                zk.exists(path, watcher);
            } catch (KeeperException e) {
                throw new ZkException(e);
            }

            final NodeInfo eni = new NodeInfo();
            eni.path = path;
            eni.watcher = watcher;
            eni.watcherOnly = true;

            nodeInfo.add(eni);
        }
    }

    public boolean removeWatcher(Object owner, String path, Watcher watcher)
            throws InterruptedException {
        final ZooKeeper zk = getZookeeper();

        synchronized (sync) {
            final List<NodeInfo> nodeInfo = nodes.get(owner);

            for (NodeInfo ni : nodeInfo) {
                if (Objects.equals(ni.path, path)) {
                    nodeInfo.remove(ni);

                    try {
                        zk.removeWatches(path, watcher, Watcher.WatcherType.Any, true);
                    } catch (KeeperException e) {
                        if (e.code() == KeeperException.Code.NOWATCHER) {
                            return false;
                        }

                        throw new ZkException(e);
                    }

                    return true;
                }
            }
        }

        return false;
    }

    public List<String> getSubPaths(String path)
            throws InterruptedException {
        final ZooKeeper zk = getZookeeper();

        try {
            return zk.getChildren(path, false);
        } catch (KeeperException e) {
            if (e.code() == KeeperException.Code.NONODE) {
                return Collections.emptyList();
            }

            throw new ZkException(e);
        }
    }

    public boolean isPathExists(String path)
            throws InterruptedException {
        final ZooKeeper zk = getZookeeper();

        try {
            return zk.exists(path, false) != null;
        } catch (KeeperException e) {
            throw new ZkException(e);
        }
    }

    public boolean isPathNotExists(String path)
            throws InterruptedException {
        final ZooKeeper zk = getZookeeper();

        try {
            return zk.exists(path, false) == null;
        } catch (KeeperException e) {
            throw new ZkException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public <T extends Serializable> T read(String path)
            throws InterruptedException {
        final ZooKeeper zk = getZookeeper();

        try {
            final Stat stat = zk.exists(path, false);
            if (stat != null) {
                final byte[] data = zk.getData(path, false, stat);
                if (data != null) {
                    return (T) SerializationUtils.deserialize(data);
                }
            }
        } catch (KeeperException e) {
            throw new ZkException(e);
        }

        return null;
    }

    public void writeIfPathExists(String path, Serializable dataToSave)
            throws InterruptedException {
        final ZooKeeper zk = getZookeeper();

        setZkData(zk, path, SerializationUtils.serialize(dataToSave));
    }

    public void writeIfPathExists(Map<String, Serializable> dataToSave)
            throws InterruptedException {
        final ZooKeeper zk = getZookeeper();

        for (Map.Entry<String, Serializable> dataEntry : dataToSave.entrySet()) {
            setZkData(zk, dataEntry.getKey(), SerializationUtils.serialize(dataEntry.getValue()));
        }
    }

    @SuppressWarnings("unused")
    @PreDestroy
    public void destroy() {
        synchronized (sync) {
            if (zooKeeper != null) {
                try {
                    zooKeeper.close();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } finally {
                    zooKeeper = null;
                }
            }
        }
    }

    private ZooKeeper getZookeeper()
            throws InterruptedException {
        synchronized (sync) {
            if (zooKeeper == null) {
                zooKeeper = tryReconnect();

                if (zooKeeper == null) {
                    throw new ZkException("Timeout when Zookeeper connection");
                }
            }

            return zooKeeper;
        }
    }

    private ZooKeeper tryReconnect()
            throws InterruptedException {
        final CountDownLatch countDownLatch = new CountDownLatch(1);

        final ZKClientConfig zkClientConfig = new ZKClientConfig();
        zkClientConfig.setProperty(ZKClientConfig.ENABLE_CLIENT_SASL_KEY, "false");

        ZooKeeper zk;
        try {
            zk = new ZooKeeper(
                    configuration.getZookeeperConnection(),
                    configuration.getZookeeperSessionTimeoutInMinutes() * 1000 * 60,
                    we -> {
                        if (we.getState() == Watcher.Event.KeeperState.SyncConnected) {
                            countDownLatch.countDown();
                        } else if (we.getState() == Watcher.Event.KeeperState.Expired) {
                            synchronized (sync) {
                                try {
                                    if(zooKeeper != null) {
                                        zooKeeper.close();
                                    }

                                    zooKeeper = tryReconnect();
                                } catch (InterruptedException e) {
                                    Thread.currentThread().interrupt();
                                } catch (Exception e) {
                                    LOG.error("Reconnect failed", e);
                                }
                            }
                        } else if (we.getState() == Watcher.Event.KeeperState.Closed) {
                            synchronized (sync) {
                                zooKeeper = null;
                            }
                        }
                    },
                    zkClientConfig
            );
        } catch (IOException e) {
            throw new ZkException(e);
        }

        if (!countDownLatch.await(1, TimeUnit.MINUTES)) {
            zk.close();

            return null;
        }

        // Пересоздание узлов и вотчеров.
        for (List<NodeInfo> nodeInfo : nodes.values()) {
            for (NodeInfo ni : nodeInfo) {
                if (ni.watcherOnly) {
                    try {
                        zk.exists(ni.path, ni.watcher);
                    } catch (KeeperException e) {
                        throw new ZkException(e);
                    }
                } else {
                    createZkPath(zk, ni.path, ni.watcher);
                }
            }
        }

        return zk;
    }

    private void setZkData(ZooKeeper zk, String path, byte[] data)
            throws InterruptedException {
        try {
            zk.setData(path, data, -1);
        } catch (KeeperException e) {
            if (e.code() == KeeperException.Code.NONODE) {
                return;
            }

            throw new ZkException(e);
        }
    }

    private void createZkPath(ZooKeeper zk, String path, Watcher watcher)
            throws InterruptedException {
        try {
            String currentPath = path;

            Stat stat = zk.exists(currentPath, watcher);
            if (stat == null) {
                final String[] pathParts = path.split("/");

                currentPath = "";
                for (int i = 1; i < pathParts.length; i++) {
                    currentPath += "/" + pathParts[i];

                    stat = zk.exists(currentPath, false);
                    if (stat == null) {
                        if (i == pathParts.length - 1) {
                            currentPath = zk.create(
                                    currentPath,
                                    null,
                                    ZooDefs.Ids.OPEN_ACL_UNSAFE,
                                    CreateMode.EPHEMERAL
                            );
                        } else {
                            currentPath = zk.create(
                                    currentPath,
                                    null,
                                    ZooDefs.Ids.OPEN_ACL_UNSAFE,
                                    CreateMode.PERSISTENT
                            );
                        }
                    }
                }
            }
        } catch (KeeperException e) {
            throw new ZkException(e);
        }
    }

    private void removeNodeInfo(ZooKeeper zk, List<NodeInfo> nodeInfo) {
        Exception error = null;
        for (NodeInfo eni : nodeInfo) {
            try {
                try {
                    if (eni.watcher != null) {
                        zk.removeWatches(eni.path, eni.watcher, Watcher.WatcherType.Any, true);
                    }
                } finally {
                    if (!eni.watcherOnly) {
                        zk.delete(eni.path, -1);
                    }
                }
            } catch(KeeperException ex) {
                if (ex.code() == KeeperException.Code.NONODE) {
                    return;
                }
            } catch (Exception ex) {
                if (ex instanceof InterruptedException) {
                    Thread.currentThread().interrupt();
                }

                if (error == null) {
                    error = ex;
                } else {
                    error.addSuppressed(ex);
                }
            }
        }

        if (error != null) {
            throw new ZkException(error);
        }
    }

    private static class NodeInfo {

        String path;

        Watcher watcher;

        boolean watcherOnly;

    }

}
