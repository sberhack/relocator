package ru.sber.hackaton.relocator.toolkit;

import java.io.*;

/**
 * Вспомогательные методы для сериализации.
 */
public final class SerializationUtils {

    private SerializationUtils() {
    }

    public static byte[] serialize(Serializable data) {
        try (
                final ByteArrayOutputStream out = new ByteArrayOutputStream();
                final ObjectOutputStream oos = new ObjectOutputStream(out)
        ) {
            oos.writeObject(data);
            out.flush();

            return out.toByteArray();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Serializable deserialize(byte[] data) {
        try (
                final ByteArrayInputStream input = new ByteArrayInputStream(data);
                final ObjectInputStream ois = new ObjectInputStream(input)
        ) {
            return (Serializable) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
