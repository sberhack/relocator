package ru.sber.hackaton.relocator.toolkit.kafka;

import org.apache.kafka.common.serialization.Serializer;
import ru.sber.hackaton.relocator.toolkit.SerializationUtils;

import java.io.Serializable;

/**
 * Сериализатор сообщений на основе Java сериализации.
 */
public class SerializableSerializer implements Serializer<Serializable> {

    @Override
    public byte[] serialize(String topic, Serializable data) {
        return SerializationUtils.serialize(data);
    }

}
