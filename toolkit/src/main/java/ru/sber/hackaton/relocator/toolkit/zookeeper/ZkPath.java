package ru.sber.hackaton.relocator.toolkit.zookeeper;

/**
 * Вспомогательные методы для пути в Zookeeper.
 */
public final class ZkPath {

    private ZkPath() {
    }

    public static String concat(String ... pathParts) {
        return String.join("/", pathParts);
    }

}
