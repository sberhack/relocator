package ru.sber.hackaton.relocator.worker.server;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SlidingTimeWindowArrayReservoir;
import com.codahale.metrics.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.TimerService;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Симуляция обработки запросов от клиента. периодически обработка запросов искусственно увеличивается.
 */
@SuppressWarnings("unused")
@Singleton
public class ThrottleJobSimulator {

    private final CountDownLatch shutdown = new CountDownLatch(1);

    @Resource
    private TimerService timerService;

    /**
     * Флаг включения периодической задержки ответа.
     */
    @Resource(name = "java:app/env/periodicThrottleJobOn")
    private Boolean periodicThrottleJobOn;

    /**
     * Флаг включения задержки ответа, если выполняется много одновременных запросов.
     */
    @Resource(name = "java:app/env/dynamicThrottleJobOn")
    private Boolean dynamicThrottleJobOn;

    /**
     * Флаг включения выдачи ошибок.
     */
    @Resource(name = "java:app/env/periodicErrorsJobOn")
    private Boolean periodicErrorsJobOn;

    private AtomicReference<Instant> periodicThrottleStarted = new AtomicReference<>();

    private AtomicReference<Instant> periodicErrorStarted = new AtomicReference<>();

    private AtomicReference<Instant> prevCallDate = new AtomicReference<>();

    private final MetricRegistry metricRegistry = new MetricRegistry();

    /**
     * Начало периодов периодической задержки ответа.
     */
    private static final long PERIODIC_START = 7 * 60 * 1000;

    /**
     * Период включения периодической нагрузки (мс).
     */
    private static final long PERIODIC_INTERVAL = 15 * 60 * 1000;

    /**
     * Длительность периодической задержки.
     */
    private static final long THROTTLE_DURATION = 3 * 60 * 1000;

    /**
     * Обычная задержка ответа.
     */
    private static final long NORMAL_TIMEOUT = 300;

    /**
     * Увеличенная задержка ответа.
     */
    private static final long THROTTLE_TIMEOUT = 2000;

    /**
     * Начало периодов выдачи ошибок.
     */
    private static final long PERIODIC_ERROR_START = 3 * 60 * 1000;

    /**
     * Период включения ошибок (мс).
     */
    private static final long PERIODIC_ERROR_INTERVAL = 10 * 60 * 1000;

    /**
     * Длительность выдачи ошибок.
     */
    private static final long ERROR_DURATION = 2 * 60 * 1000;

    private static final long DYNAMIC_THRESHOLD = 380;

    private static final Logger LOG = LoggerFactory.getLogger(ThrottleJobSimulator.class);

    @PostConstruct
    public void init() {
        LOG.info("ThrottleJobSimulator start, periodicThrottleJobOn: {}, " +
                        "dynamicThrottleJobOn: {}, periodicErrorsJobOn: {}",
                periodicThrottleJobOn, dynamicThrottleJobOn, periodicErrorsJobOn);

        if (periodicThrottleJobOn != null && periodicThrottleJobOn) {
            timerService.createTimer(PERIODIC_START, PERIODIC_INTERVAL, "throttle");
        }

        if (periodicErrorsJobOn != null && periodicErrorsJobOn) {
            timerService.createTimer(PERIODIC_ERROR_START, PERIODIC_ERROR_INTERVAL, "error");
        }
    }

    @PreDestroy
    public void destroy() {
        shutdown.countDown();
    }

    @Timeout
    public void setThrottle(javax.ejb.Timer timer) {
        LOG.info("Periodic {}} start", timer.getInfo());

        if(Objects.equals(timer.getInfo(), "throttle")) {
            periodicThrottleStarted.set(Instant.now());
        } else if(Objects.equals(timer.getInfo(), "error")) {
            periodicErrorStarted.set(Instant.now());
        }
    }

    @SuppressWarnings("WeakerAccess")
    public void job() {
        final Timer meter = metricRegistry.timer(
                "ThrottleJobSimulator",
                () -> new Timer(new SlidingTimeWindowArrayReservoir(1, TimeUnit.MINUTES))
        );
        if (prevCallDate.get() != null) {
            meter.update(Duration.between(prevCallDate.get(), Instant.now()).toMillis(), TimeUnit.MILLISECONDS);
        }

        final long prevMillis = Duration.of(
                (long) meter.getSnapshot().getMean(), ChronoUnit.NANOS
        ).toMillis();

        Instant periodicThrottleStartedDate = periodicThrottleStarted.get();
        if (periodicThrottleStartedDate != null) {
            if (Duration.between(periodicThrottleStartedDate, Instant.now()).toMillis() > THROTTLE_DURATION) {
                periodicThrottleStartedDate = null;

                LOG.info("Periodic throttle finish");

                periodicThrottleStarted.set(null);
            }
        }

        final long throttle;
        if (periodicThrottleStartedDate != null ||
                (dynamicThrottleJobOn != null && dynamicThrottleJobOn && prevMillis <= DYNAMIC_THRESHOLD)
        ) {
            if (periodicThrottleStartedDate != null) {
                LOG.info("Do job, periodic throttle on, prevCall: {}", prevMillis);
            }

            if (dynamicThrottleJobOn != null && dynamicThrottleJobOn) {
                LOG.info("Do job, dynamic throttle on, prevCall: {}", prevMillis);
            }

            throttle = THROTTLE_TIMEOUT;
        } else {
            LOG.info("Do job, normal duration, prevCall: {}", prevMillis);

            throttle = NORMAL_TIMEOUT;
        }

        try {
            shutdown.await(throttle, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        prevCallDate.set(Instant.now());

        Instant periodicErrorStartedDate = periodicErrorStarted.get();
        if (periodicErrorStartedDate != null) {
            if (Duration.between(periodicErrorStartedDate, Instant.now()).toMillis() > ERROR_DURATION) {
                periodicErrorStartedDate = null;

                LOG.info("Periodic error finish");

                periodicErrorStarted.set(null);
            }
        }

        if (periodicErrorStartedDate != null) {
            throw new RuntimeException("periodic error");
        }
    }

}
