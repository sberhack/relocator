package ru.sber.hackaton.relocator.worker.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.enterprise.concurrent.ManagedThreadFactory;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Симуляция нагрузки на сервер, которая не зависит от клиентов. Например выполнение периодических задач,
 * связанных с обслуживанием БД.
 */
@SuppressWarnings("unused")
@Startup
@Singleton
public class HeavyJobSimulator {

    @Resource
    private TimerService timerService;

    @Resource
    private ManagedThreadFactory threadFactory;

    @Resource(name = "java:app/env/heavyJobOn")
    private Boolean heavyJobOn;

    private final CountDownLatch shutdown = new CountDownLatch(1);

    private static final long START = 2 * 60 * 1000;

    /**
     * Период включения нагрузки (мс).
     */
    private static final long INTERVAL = 15 * 60 * 1000;

    private static final Logger LOG = LoggerFactory.getLogger(HeavyJobSimulator.class);

    @PostConstruct
    public void init() {
        if (heavyJobOn) {
            timerService.createTimer(START, INTERVAL, null);
        }
    }

    @PreDestroy
    public void destroy() {
        for (Timer timer : timerService.getTimers()) {
            timer.cancel();
        }

        shutdown.countDown();
    }

    @Timeout
    public void job() {
        LOG.info("Start heavy job");

        threadFactory.newThread(() -> {
            for (int i = 0; i < 180; i++) {
                try {
                    final double[] data = new double[1024 * 1024 * 5];
                    for (int j = 0; j < data.length; j++) {
                        data[j] = Math.atan2(j, data.length - j);
                    }

                    double result = 0;
                    for (double datum : data) {
                        result += datum;
                    }

                    LOG.trace("Result: {}", result);

                    if (shutdown.await(1000, TimeUnit.MILLISECONDS)) {
                        break;
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();

                    break;
                }
            }

            LOG.info("Finish heavy job");
        }).start();
    }

}
