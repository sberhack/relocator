package ru.sber.hackaton.relocator.worker.server;

import ru.sber.hackaton.relocator.transport.ServerRegistry;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 * Регистрация сервера.
 */
@SuppressWarnings("unused")
@Startup
@Singleton
public class Bootstrap {

    @Inject
    private ServerRegistry serverRegistry;

    @Inject
    private WorkerServer workerServer;

    @PostConstruct
    public void start() {
        // Регистрация сервера, теперь он доступен по сети по адресу "server".
        try {
            serverRegistry.register("server", workerServer);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    @PreDestroy
    public void destroy() {
        // Выключаем сервер, теперь оне недоступен.
        serverRegistry.remove(workerServer);
    }

}
