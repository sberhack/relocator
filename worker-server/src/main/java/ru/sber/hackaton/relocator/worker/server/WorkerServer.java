package ru.sber.hackaton.relocator.worker.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sber.hackaton.relocator.worker.server.api.Worker;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.Instant;
import java.util.concurrent.CompletableFuture;

/**
 * Сервер, доступный через межмодульное взаимодействие.
 */
@SuppressWarnings("unused")
@ApplicationScoped
public class WorkerServer implements Worker {

    @Inject
    private ThrottleJobSimulator throttleJobSimulator;

    private static final Logger LOG = LoggerFactory.getLogger(WorkerServer.class);

    @Override
    public CompletableFuture<String> callService(String parameter) {
        LOG.info("Request, parameter: {}", parameter);

        throttleJobSimulator.job();

        final String response = parameter + "->" + Instant.now().toString();

        return CompletableFuture.completedFuture(response);
    }

}
