package ru.sber.hackaton.relocator.worker.server.api;

import ru.sber.hackaton.relocator.transport.Api;

import java.util.concurrent.CompletableFuture;

/**
 * Интерфейс, доступный через межмодульное взаимодействие.
 */
@Api
public interface Worker {

    CompletableFuture<String> callService(String parameter);

}
