#!/bin/bash

cd ../worker-server-module || exit 1
mvn wildfly:deploy -Dwildfly.port=9993
mvn wildfly:deploy -Dwildfly.port=9994 -DheavyJobOn=true
mvn wildfly:deploy -Dwildfly.port=9995 -DperiodicThrottleJobOn=true
mvn wildfly:deploy -Dwildfly.port=9996 -DperiodicErrorsJobOn=true
