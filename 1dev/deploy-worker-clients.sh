#!/bin/bash

cd ../worker-client-module || exit 1
mvn wildfly:deploy -Dwildfly.port=9992 -DclientModuleId=worker-client01
mvn wildfly:deploy -Dwildfly.port=9992 -DclientModuleId=worker-client02
mvn wildfly:deploy -Dwildfly.port=9992 -DclientModuleId=worker-client03
mvn wildfly:deploy -Dwildfly.port=9992 -DclientModuleId=worker-client04
mvn wildfly:deploy -Dwildfly.port=9992 -DclientModuleId=worker-client05
mvn wildfly:deploy -Dwildfly.port=9992 -DclientModuleId=worker-client06
mvn wildfly:deploy -Dwildfly.port=9992 -DclientModuleId=worker-client07
mvn wildfly:deploy -Dwildfly.port=9992 -DclientModuleId=worker-client08
mvn wildfly:deploy -Dwildfly.port=9992 -DclientModuleId=worker-client09
mvn wildfly:deploy -Dwildfly.port=9992 -DclientModuleId=worker-client10
mvn wildfly:deploy -Dwildfly.port=9992 -DclientModuleId=worker-client11
