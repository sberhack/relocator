1. Нужен ansible:

(Linux/Windows)
sudo apt update
sudo apt -y upgrade
sudo apt-add-repository --yes ppa:ansible/ansible
sudo apt update
sudo apt -y install ansible

(Mac OS)
brew install ansible

2. Настройка среды разработчика:

sudo ansible-playbook setup.yaml

3. Дополнительные действия:

(Windows)
echo "export DOCKER_HOST=tcp://localhost:2375" >> ~/.bashrc && source ~/.bashrc

Настроить пользователя Mongo Charts -
docker exec -it 1dev_mongo-charts01_1 bash
charts-cli add-user --first-name "First" --last-name "Last" --email "stats@stats.com" --password "sberhack" --role "UserAdmin"
