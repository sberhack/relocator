package ru.sber.hackaton.relocator.chief;

import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.model.ReplaceOneModel;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoCollection;
import com.mongodb.reactivestreams.client.MongoDatabase;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.codecs.ValueCodecProvider;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.jsr310.Jsr310CodecProvider;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sber.hackaton.relocator.transport.internal.ServerRecord;

import javax.annotation.PreDestroy;
import javax.enterprise.context.Dependent;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

/**
 * Хранилище статистки по северам.
 */
@Dependent
public class ServerStatStorage {

    private final static String MONGO_CONNECT = "MONGO_CONNECT";

    /**
     * База данных статистики.
     */
    private final static String DB = "stats";

    /**
     * Префикс коллекции со статистикой по серверам. Суффикс - имя узла, на которых находятся сервера.
     */
    private final static String SERVER_STAT_COLLECTION = "serverStats-";

    /**
     * Коллекция серверов.
     */
    private final static String SERVER_NAME_COLLECTION = "serverName";

    /**
     * Коллекция состояний серверов.
     */
    private final static String SERVER_STATE_COLLECTION = "serverState";

    private final MongoClient mongoClient;

    private final MongoDatabase statsDatabase;

    /**
     * Максимальный интервал, после которого сервер считается отключенным и статистика с него не собирается.
     */
    private final static int TIME_MINUTES_SHIFT = 2;

    private static final ReplaceOptions BULK_UPSERT_OPERATION = new ReplaceOptions().upsert(true);

    private static final Logger LOG = LoggerFactory.getLogger(ServerStatStorage.class);

    public ServerStatStorage() {
        mongoClient = MongoClients.create(System.getenv(MONGO_CONNECT));
        statsDatabase = mongoClient.getDatabase(DB).withCodecRegistry(CodecRegistries
                .fromProviders(
                        PojoCodecProvider.builder()
                                .register(ServerStat.class)
                                .register(ServerState.class)
                                .register(ServerName.class)
                                .register(Document.class)
                                .register(BsonDocument.class)
                                .build(),
                        new Jsr310CodecProvider(),
                        new ValueCodecProvider()
                )
        );
    }

    @SuppressWarnings("unused")
    @PreDestroy
    public void destroy() {
        if (mongoClient != null) {
            mongoClient.close();
        }
    }

    /**
     * Возвращает список имен серверов.
     *
     * @return Имена серверов.
     */
    CompletableFuture<List<ServerName>> getNodeNames() {
        final ListNameSubscriber listNameSubscriber = new ListNameSubscriber();

        statsDatabase.getCollection(SERVER_NAME_COLLECTION, ServerName.class)
                .find().subscribe(listNameSubscriber);

        return listNameSubscriber;
    }

    /**
     * Возвращает статистику за период.
     *
     * @return Статистика.
     */
    CompletableFuture<List<ServerStat>> getStat(String node, LocalDateTime startDate, LocalDateTime endDate) {
        final ListStatSubscriber listStatSubscriber = new ListStatSubscriber();

        statsDatabase.getCollection(SERVER_STAT_COLLECTION + node, ServerStat.class)
                .find(and(gte("_id", startDate), lt("_id", endDate)))
                .subscribe(listStatSubscriber);

        return listStatSubscriber;
    }

    /**
     * Возвращает текущее состояние серверов.
     *
     * @return Текущее состояние серверов.
     */
    CompletableFuture<List<ServerState>> getStates() {
        final ListStateSubscriber listStateSubscriber = new ListStateSubscriber();

        statsDatabase.getCollection(SERVER_STATE_COLLECTION, ServerState.class)
                .find().subscribe(listStateSubscriber);

        return listStateSubscriber;
    }

    /**
     * Обновляем статусы узлов в БД.
     *
     * @return Отслеживание результата.
     */
    CompletableFuture<Void> writeStates(Collection<ServerState> serverStats) {
        final MongoCollection<ServerState> nodeCollection =
                statsDatabase.getCollection(
                        SERVER_STATE_COLLECTION, ServerState.class
                );
        final BulkWriteSubscriber bulkWriteSubscriber = new BulkWriteSubscriber();

        nodeCollection.bulkWrite(serverStats.stream().map(x -> new ReplaceOneModel<>(
                eq("_id", x.getId()),
                x,
                BULK_UPSERT_OPERATION
        )).collect(Collectors.toList())).subscribe(bulkWriteSubscriber);

        return bulkWriteSubscriber;
    }

    /**
     * Сохранение статистики по сервера в базу данных.
     *
     * @param serverRecordsOnNodes Список узлов + список идентификаторов серверов, расположенных на этих узлах.
     */
    void save(Map<String, List<Stat>> serverRecordsOnNodes) {
        final Instant now = Instant.now();

        // Грануляция сбора статистики - 1 минута.
        final LocalDateTime currentMinute =
                LocalDateTime.ofInstant(now, ZoneId.systemDefault())
                        .truncatedTo(ChronoUnit.MINUTES);

        final Map<String, ServerStat> serverStatMap = new HashMap<>();
        for (Map.Entry<String, List<Stat>> serverRecordsOnNode : serverRecordsOnNodes.entrySet()) {
            for (Stat stat : serverRecordsOnNode.getValue()) {
                if (Duration.between(stat.serverRecord.getDate(), now).toMinutes() >= TIME_MINUTES_SHIFT) {
                    // Отбрасываем не обновляемые записи.
                    continue;
                }

                final ServerStat serverStat = serverStatMap.computeIfAbsent(
                        serverRecordsOnNode.getKey(), key -> new ServerStat()
                );

                updateStatFromServerRecord(serverStat, stat);
            }
        }

        // Обновляем статистику узлов в БД.
        for (Map.Entry<String, ServerStat> entry : serverStatMap.entrySet()) {
            final MongoCollection<ServerStat> statCollection =
                    statsDatabase.getCollection(
                            SERVER_STAT_COLLECTION + entry.getKey(), ServerStat.class
                    );

            statCollection.find(eq("_id", currentMinute))
                    .first()
                    .subscribe(new ReadSubscriber(currentMinute, statCollection, entry.getValue()));
        }

        // Обновляем имена узлов в БД.
        final MongoCollection<ServerName> nodeCollection =
                statsDatabase.getCollection(
                        SERVER_NAME_COLLECTION, ServerName.class
                );
        nodeCollection.bulkWrite(serverStatMap.keySet().stream().map(x -> new ReplaceOneModel<>(
                eq("_id", x),
                new ServerName(x),
                BULK_UPSERT_OPERATION
        )).collect(Collectors.toList())).subscribe(new BulkWriteSubscriber());
    }

    private static void updateStatFromServerRecord(ServerStat serverStat, Stat stat) {
        if (serverStat.getChangeAmount() == 0) {
            // Первоначальная установка.
            serverStat.setUsedMemory(stat.serverRecord.getUsedMemory());
            serverStat.setProcessorLoading(stat.serverRecord.getProcessorLoading());
            serverStat.setChangeAmount(1);
        } else {
            // Считаем среднее.
            serverStat.setUsedMemory(calcMiddle(
                    serverStat.getUsedMemory(),
                    stat.serverRecord.getUsedMemory(),
                    serverStat.getChangeAmount()
            ));
            serverStat.setProcessorLoading(calcMiddle(
                    serverStat.getProcessorLoading(),
                    stat.serverRecord.getProcessorLoading(),
                    serverStat.getChangeAmount()
            ));

            serverStat.setChangeAmount(serverStat.getChangeAmount() + 1);
        }

        serverStat.setMaxMemory(
                stat.serverRecord.getMaxMemory()
        );
        serverStat.setOneMinuteRateRequest(
                serverStat.getOneMinuteRateRequest() +
                        stat.serverRecord.getOneMinuteRateRequest()
        );
        serverStat.setOneMinuteTimeRequest(
                serverStat.getOneMinuteTimeRequest() +
                        stat.serverRecord.getOneMinuteTimeRequest()
        );
        serverStat.setOneMinuteRateError(
                serverStat.getOneMinuteRateError() +
                        stat.serverRecord.getOneMinuteRateError()
        );
        serverStat.setOneMinuteGCTime(
                serverStat.getOneMinuteGCTime() +
                        stat.serverRecord.getOneMinuteGCTime()
        );
        serverStat.setOneMinuteGCCount(
                serverStat.getOneMinuteGCCount() +
                        stat.serverRecord.getOneMinuteGCCount()
        );
        serverStat.setClientAmount(
                serverStat.getClientAmount() +
                        stat.clientAmount
        );
    }

    private static void updateSavedStat(ServerStat serverStat, ServerStat newServerStat) {
        serverStat.setUsedMemory(calcMiddle(
                serverStat.getUsedMemory(),
                newServerStat.getUsedMemory(),
                serverStat.getChangeAmount()
        ));
        serverStat.setProcessorLoading(calcMiddle(
                serverStat.getProcessorLoading(),
                newServerStat.getProcessorLoading(),
                serverStat.getChangeAmount()
        ));

        serverStat.setOneMinuteRateRequest(
                newServerStat.getOneMinuteRateRequest()
        );
        serverStat.setOneMinuteTimeRequest(
                newServerStat.getOneMinuteTimeRequest()
        );
        serverStat.setOneMinuteRateError(
                newServerStat.getOneMinuteRateError()
        );
        serverStat.setOneMinuteGCTime(
                newServerStat.getOneMinuteGCTime()
        );
        serverStat.setOneMinuteGCCount(
                newServerStat.getOneMinuteGCCount()
        );
        serverStat.setClientAmount(
                newServerStat.getClientAmount()
        );

        serverStat.setChangeAmount(serverStat.getChangeAmount() + 1);
    }

    private static double calcMiddle(double prevValue, double newValue, int count) {
        return prevValue * ((double) count / (count + 1d)) + newValue * (1d / (count + 1));
    }

    private static class ReadSubscriber implements Subscriber<ServerStat> {

        final MongoCollection<ServerStat> nodeCollection;

        final LocalDateTime currentMinute;

        final ServerStat serverStat;

        ServerStat dbServerStat;

        private ReadSubscriber(
                LocalDateTime currentMinute,
                MongoCollection<ServerStat> nodeCollection,
                ServerStat serverStat
        ) {
            this.currentMinute = currentMinute;
            this.nodeCollection = nodeCollection;
            this.serverStat = serverStat;
        }

        @Override
        public void onSubscribe(Subscription s) {
            s.request(1);
        }

        @Override
        public void onNext(ServerStat serverStat) {
            dbServerStat = serverStat;
        }

        @Override
        public void onError(Throwable t) {
            LOG.error("Failed to read database", t);
        }

        @Override
        public void onComplete() {
            if (dbServerStat != null) {
                // Обновляем предыдущее значение.
                updateSavedStat(dbServerStat, serverStat);

                nodeCollection.updateOne(
                        eq("_id", currentMinute),
                        combine(
                                set("changeAmount", dbServerStat.getChangeAmount()),
                                set("usedMemory", dbServerStat.getUsedMemory()),
                                set("processorLoading", dbServerStat.getProcessorLoading()),
                                set("oneMinuteRateRequest", dbServerStat.getOneMinuteRateRequest()),
                                set("oneMinuteTimeRequest", dbServerStat.getOneMinuteTimeRequest()),
                                set("oneMinuteRateError", dbServerStat.getOneMinuteRateError()),
                                set("oneMinuteGCTime", dbServerStat.getOneMinuteGCTime()),
                                set("oneMinuteGCCount", dbServerStat.getOneMinuteGCCount()),
                                set("clientAmount", dbServerStat.getClientAmount())
                        )
                ).subscribe(new UpdateSubscriber<>());
            } else {
                // Нужна вставка.
                serverStat.setId(currentMinute);
                nodeCollection.insertOne(
                        serverStat
                ).subscribe(new InsertSubscriber<>());
            }
        }

    }

    private static class InsertSubscriber<T> implements Subscriber<T> {

        @Override
        public void onSubscribe(Subscription s) {
            s.request(1);
        }

        @Override
        public void onNext(T success) {
        }

        @Override
        public void onError(Throwable t) {
            LOG.error("Failed save in database", t);
        }

        @Override
        public void onComplete() {
        }

    }

    private static class UpdateSubscriber<T> implements Subscriber<T> {

        @Override
        public void onSubscribe(Subscription s) {
            s.request(1);
        }

        @Override
        public void onNext(T success) {
        }

        @Override
        public void onError(Throwable t) {
            LOG.error("Failed save in database", t);
        }

        @Override
        public void onComplete() {
        }

    }

    private static class ListNameSubscriber
            extends CompletableFuture<List<ServerName>>
            implements Subscriber<ServerName> {

        private final List<ServerName> names = new ArrayList<>();

        @Override
        public void onSubscribe(Subscription s) {
            s.request(Long.MAX_VALUE);
        }

        @Override
        public void onNext(ServerName name) {
            names.add(name);
        }

        @Override
        public void onError(Throwable t) {
            LOG.error("Failed read database", t);

            completeExceptionally(t);
        }

        @Override
        public void onComplete() {
            complete(names);
        }

    }

    private static class ListStatSubscriber
            extends CompletableFuture<List<ServerStat>>
            implements Subscriber<ServerStat> {

        private final List<ServerStat> stats = new ArrayList<>();

        @Override
        public void onSubscribe(Subscription s) {
            s.request(Long.MAX_VALUE);
        }

        @Override
        public void onNext(ServerStat state) {
            stats.add(state);
        }

        @Override
        public void onError(Throwable t) {
            LOG.error("Failed read database", t);

            completeExceptionally(t);
        }

        @Override
        public void onComplete() {
            complete(stats);
        }

    }

    private static class ListStateSubscriber
            extends CompletableFuture<List<ServerState>>
            implements Subscriber<ServerState> {

        private final List<ServerState> states = new ArrayList<>();

        @Override
        public void onSubscribe(Subscription s) {
            s.request(Long.MAX_VALUE);
        }

        @Override
        public void onNext(ServerState state) {
            states.add(state);
        }

        @Override
        public void onError(Throwable t) {
            LOG.error("Failed read database", t);

            completeExceptionally(t);
        }

        @Override
        public void onComplete() {
            complete(states);
        }

    }

    private static class BulkWriteSubscriber
            extends CompletableFuture<Void>
            implements Subscriber<BulkWriteResult> {

        @Override
        public void onSubscribe(Subscription s) {
            s.request(Integer.MAX_VALUE);
        }

        @Override
        public void onNext(BulkWriteResult bulkWriteResult) {
        }

        @Override
        public void onError(Throwable t) {
            LOG.error("Failed write database", t);

            completeExceptionally(t);
        }

        @Override
        public void onComplete() {
            complete(null);
        }

    }

    static class Stat {

        private final ServerRecord serverRecord;

        private final int clientAmount;

        Stat(ServerRecord serverRecord, int clientAmount) {
            this.serverRecord = serverRecord;
            this.clientAmount = clientAmount;
        }

    }

}
