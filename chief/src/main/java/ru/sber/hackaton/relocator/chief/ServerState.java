package ru.sber.hackaton.relocator.chief;

/**
 * Текущее состояние сервера, сохраняется в БД.
 */
public class ServerState {

    public enum State {

        RED,

        ORANGE,

        YELLOW,

        GREEN,

    }

    public ServerState() {
    }

    public ServerState(String id) {
        this.id = id;
    }

    private String id;

    private State state = State.GREEN;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

}
