package ru.sber.hackaton.relocator.chief;

import java.time.LocalDateTime;

/**
 * Статистические данные по серверу, сохраняются в БД
 */
public class ServerStat {

    /**
     * Дата сбора статистики.
     */
    private LocalDateTime id;

    /**
     * Максимальный размер доступной памяти.
     */
    private double maxMemory;

    /**
     * Память, используемая сервером.
     */
    private double usedMemory;

    /**
     * Текущая загрузка процессора.
     */
    private double processorLoading;

    /**
     * Количество запросов в минуту.
     */
    private double oneMinuteRateRequest;

    /**
     * Среднее время отклика сервера за минуту.
     */
    private double oneMinuteTimeRequest;

    /**
     * Количество отказов в минуту.
     */
    private double oneMinuteRateError;

    /**
     * Скорость изменения времени, потраченного на сборку мусора, в минуту.
     */
    private double oneMinuteGCTime;

    /**
     * Скорость изменения количество сборов мусора, в минуту.
     */
    private double oneMinuteGCCount;

    /**
     * Текущее кол-во подклюыенных клиентов.
     */
    private double clientAmount;

    /**
     * Количество изменений для подсчета среднего.
     */
    private int changeAmount = 0;

    public LocalDateTime getId() {
        return id;
    }

    public void setId(LocalDateTime id) {
        this.id = id;
    }

    public double getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(double maxMemory) {
        this.maxMemory = maxMemory;
    }

    public double getUsedMemory() {
        return usedMemory;
    }

    public void setUsedMemory(double usedMemory) {
        this.usedMemory = usedMemory;
    }

    public double getProcessorLoading() {
        return processorLoading;
    }

    public void setProcessorLoading(double processorLoading) {
        this.processorLoading = processorLoading;
    }

    public double getOneMinuteRateRequest() {
        return oneMinuteRateRequest;
    }

    public void setOneMinuteRateRequest(double oneMinuteRateRequest) {
        this.oneMinuteRateRequest = oneMinuteRateRequest;
    }

    public double getOneMinuteTimeRequest() {
        return oneMinuteTimeRequest;
    }

    public void setOneMinuteTimeRequest(double oneMinuteTimeRequest) {
        this.oneMinuteTimeRequest = oneMinuteTimeRequest;
    }

    public double getOneMinuteRateError() {
        return oneMinuteRateError;
    }

    public void setOneMinuteRateError(double oneMinuteRateError) {
        this.oneMinuteRateError = oneMinuteRateError;
    }

    public double getOneMinuteGCTime() {
        return oneMinuteGCTime;
    }

    public void setOneMinuteGCTime(double oneMinuteGCTime) {
        this.oneMinuteGCTime = oneMinuteGCTime;
    }

    public double getOneMinuteGCCount() {
        return oneMinuteGCCount;
    }

    public void setOneMinuteGCCount(double oneMinuteGCCount) {
        this.oneMinuteGCCount = oneMinuteGCCount;
    }

    public double getClientAmount() {
        return clientAmount;
    }

    public void setClientAmount(double clientAmount) {
        this.clientAmount = clientAmount;
    }

    public int getChangeAmount() {
        return changeAmount;
    }

    public void setChangeAmount(int changeAmount) {
        this.changeAmount = changeAmount;
    }

}
