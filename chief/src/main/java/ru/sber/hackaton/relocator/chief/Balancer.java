package ru.sber.hackaton.relocator.chief;

import ru.sber.hackaton.relocator.toolkit.zookeeper.ZkClient;
import ru.sber.hackaton.relocator.transport.internal.Paths;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Диспетчер клиентов.
 */
@SuppressWarnings("unused")
@Startup
@Singleton
public class Balancer {

    @Inject
    private ZkClient zkClient;

    @Inject
    private ServerStatStorage serverStatStorage;

    @Schedule(second = "*/30", minute = "*", hour = "*", persistent = false)
    public void rebalance()
            throws InterruptedException {
        // Карта узлов и серверов на этом узле (ID сервера -> сервер).
        final Map<String, List<Server>> serverMap = buildServerMap();

        serverStatStorage.getStates()
                .thenAccept(states -> {
                    assignServerStates(serverMap, states);

                    distributeSlots(serverMap);

                    distributeClientsBySlots(serverMap);

                    try {
                        updateClientRoutes(serverMap);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                });
    }

    private Map<String, List<Server>> buildServerMap()
            throws InterruptedException {
        final Map<String, List<Server>> serverMap = new HashMap<>();

        // Получаем живые сервера.
        final Map<String, List<String>> activeServers = Paths.getActiveServers(zkClient);

        // Строим карту соотвествия идентификатора сервера и списка серверов с этим идентификатором.
        for (Map.Entry<String, List<String>> activeServerEntry : activeServers.entrySet()) {
            final Map<String, Map<String, List<String>>> clientsOnNode =
                    Paths.getClientsConnectedToSpecifiedNode(zkClient, activeServerEntry.getKey());

            for (String serverId : activeServerEntry.getValue()) {
                final List<Server> servers = serverMap.computeIfAbsent(
                        serverId, key -> new ArrayList<>()
                );

                final Server server = new Server(activeServerEntry.getKey(), serverId);
                servers.add(server);

                // Ищем текущих клиентов заданного сервера.
                for (Map.Entry<String, Map<String, List<String>>> clientModulesOnNode : clientsOnNode.entrySet()) {
                    for (Map.Entry<String, List<String>> clientModuleOnNode : clientModulesOnNode.getValue().entrySet()) {
                        for (String clientServerId : clientModuleOnNode.getValue()) {
                            if (serverId.equals(clientServerId)) {
                                server.clients.add(new Client(
                                        server, clientModulesOnNode.getKey(), clientModuleOnNode.getKey()
                                ));
                            }
                        }
                    }
                }
            }
        }

        return serverMap;
    }

    private void assignServerStates(Map<String, List<Server>> serverMap, List<ServerState> states) {
        for (Map.Entry<String, List<Server>> serverIdToServers : serverMap.entrySet()) {
            serverIdToServers.getValue().forEach(
                    x -> states.stream()
                            .filter(s -> s.getId().equals(x.nodeId))
                            .forEach(s -> x.serverState = s)
            );
        }
    }

    /**
     * Распределение слотов для клиентов по весам загруженности серверов (крсный - малый вес, зеленый - самый большой).
     *
     * @param serverMap Карта серверов для распределения по ней слотов клиентов.
     */
    private void distributeSlots(Map<String, List<Server>> serverMap) {
        for (Map.Entry<String, List<Server>> serverIdToServers : serverMap.entrySet()) {
            final int total = serverIdToServers.getValue().stream()
                    .map(x -> x.clients.size())
                    .reduce(0, Integer::sum);
            final double distributionSum = serverIdToServers.getValue().stream()
                    .map(x -> x.serverState.getState().ordinal())
                    .reduce(0, Integer::sum);

            for (Server server : serverIdToServers.getValue()) {
                final double p = (double) server.serverState.getState().ordinal() / distributionSum;
                final double weightedValue = p * total;

                server.slots = (int)Math.ceil(weightedValue);
            }
        }
    }

    /**
     * Перераспределение клиентов согласно слотам.
     *
     * @param serverMap Карта серверов.
     */
    private void distributeClientsBySlots(Map<String, List<Server>> serverMap) {
        for (Map.Entry<String, List<Server>> serverIdToServers : serverMap.entrySet()) {
            // Собираем всех клиентов текущего ID сервера.
            final List<Client> clients = serverIdToServers.getValue().stream()
                    .flatMap(x -> x.clients.stream())
                    .collect(Collectors.toList());

            // Строим отсортированную карту расстояний между сервером и потенциальными клиентами.
            final List<ServerClientDistance> distanceMap = new ArrayList<>();
            for (Server server : serverIdToServers.getValue()) {
                distanceMap.add(new ServerClientDistance(
                        server,
                        clients.stream().map(c -> new ClientDistance(
                                        server.nodeId.equals(c.nodeId) ? -1 : Math.abs(server.zone - c.zone), c
                                )
                        ).sorted(Comparator.comparingInt(c -> c.distance)).collect(Collectors.toList())
                ));
            }

            // Распределяем клиентов по серверам (количество слотов всегда либо равно либо чуть больше чем клиентов).
            boolean assignOccurs;
            do {
                // Ищем клиента и сервера с минимальным расстоянием.
                final Optional<ServerClientDistance> entry =
                        distanceMap.stream().filter(
                                // Сервера у которых еще остались потенциальные клиенты и свободные слоты.
                                x -> x.server.slots > 0 && !x.clientDistances.isEmpty()
                        ).min((x1, x2) -> {
                            final ClientDistance c1 =
                                    x1.clientDistances.get(0);
                            final ClientDistance c2 =
                                    x2.clientDistances.stream().filter(x -> !c1.equals(x)).findFirst().orElse(null);
                            if (c2 == null) {
                                // Не найден второй клиент для сравнения, считаем что первый всегда меньше.
                                return -1;
                            }

                            return Integer.compare(c1.distance, c2.distance);
                        });
                entry.ifPresent(e -> {
                    // Найден клиент и сервер с минимальным расстоянием.
                    final ClientDistance clientDistance = e.clientDistances.get(0);

                    if (!e.server.clients.contains(clientDistance.client)) {
                        // Если клиент не был в списке для этого сервера - добавляем в список на переключение.
                        e.server.newClients.add(clientDistance.client);
                    }

                    // Исключаем клиента из кандидатов у серверов.
                    distanceMap.forEach(x -> x.clientDistances.removeIf(c -> clientDistance.client.equals(c.client)));

                    e.server.slots--;
                });

                assignOccurs = entry.isPresent();
            } while (assignOccurs);
        }
    }

    /**
     * Переключаем клиенты на заданные сервера.
     *
     * @param serverMap Карта серверов.
     * @throws InterruptedException Поток прерывается.
     */
    private void updateClientRoutes(Map<String, List<Server>> serverMap)
            throws InterruptedException {
        // Переключаем клиенты на заданные сервера.
        for (Map.Entry<String, List<Server>> serverIdToServers : serverMap.entrySet()) {
            for (Server server : serverIdToServers.getValue()) {
                for (Client clientToMove : server.newClients) {
                    zkClient.writeIfPathExists(Paths.getClientPathOnNode(
                            clientToMove.nodeId, server.serverId, clientToMove.moduleId
                    ), server.nodeId);
                }
            }
        }
    }

    private static class Server {

        final String nodeId;

        final String serverId;

        final List<Client> clients = new ArrayList<>();

        final List<Client> newClients = new ArrayList<>();

        final int zone;

        ServerState serverState = new ServerState();

        int slots;

        Server(String nodeId, String serverId) {
            this.nodeId = nodeId;
            this.serverId = serverId;
            this.zone = Paths.getZoneId(nodeId);
        }

    }

    private static class Client {

        final Server server;

        final String nodeId;

        final String moduleId;

        final int zone;

        Client(Server server, String nodeId, String moduleId) {
            this.server = server;
            this.nodeId = nodeId;
            this.moduleId = moduleId;
            this.zone = Paths.getZoneId(nodeId);
        }

    }

    private static class ClientDistance {

        final int distance;

        final Client client;

        ClientDistance(int distance, Client client) {
            this.distance = distance;
            this.client = client;
        }

    }

    private static class ServerClientDistance {

        final Server server;

        final List<ClientDistance> clientDistances;

        ServerClientDistance(Server server, List<ClientDistance> clientDistances) {
            this.server = server;
            this.clientDistances = clientDistances;
        }

    }

}
