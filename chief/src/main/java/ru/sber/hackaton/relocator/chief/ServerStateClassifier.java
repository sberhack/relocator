package ru.sber.hackaton.relocator.chief;

import com.sun.jna.IntegerType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * Сервис классификации серверов по загруженности.
 */
@SuppressWarnings("unused")
@Singleton
public class ServerStateClassifier {

    @Inject
    private ServerStatStorage serverStatStorage;

    private static final OutlierChecker REQUEST_TIME_CHECKER = new OutlierChecker(100.0);

    private static final OutlierChecker GC_COUNT_CHECKER = new OutlierChecker(0.1);

    // TODO Задать настройкой.
    private static final int statWindow = 1;

    // TODO Задать настройкой.
    private static final TemporalUnit statWindowType = ChronoUnit.HOURS;

    private static final Logger LOG = LoggerFactory.getLogger(ServerStateClassifier.class);

    @Schedule(second = "*/7", minute = "*", hour = "*", persistent = false)
    public void classify() {
        final Context ctx = new Context();
        final LocalDateTime now = LocalDateTime.now();

        CompletableFuture.allOf(
                serverStatStorage.getStates().thenAccept(states -> ctx.states = states.stream().collect(
                        Collectors.toConcurrentMap(ServerState::getId, x -> x))
                ),
                serverStatStorage.getNodeNames().thenAccept(names -> ctx.names = names)
        ).thenCompose(aVoid -> CompletableFuture.allOf(ctx.names.stream().map(x ->
                serverStatStorage.getStat(x.getId(), now.minus(statWindow, statWindowType), now)
                        .thenAccept(stat -> {
                            // Получаем статистику в заданное окно.
                            final ServerState state = ctx.states.computeIfAbsent(
                                    x.getId(), key -> new ServerState(x.getId())
                            );

                            final List<ServerState.State> calculatedStates = new ArrayList<>();

                            // Ищем выбросы и их степень по задержке ответа сервера.
                            calculatedStates.add(
                                    REQUEST_TIME_CHECKER.hasOutlier(
                                            stat.stream().mapToDouble(ServerStat::getOneMinuteTimeRequest).toArray()
                                    )
                            );

                            // Ищем выбросы и их степень по количеству сборок мусора.
                            calculatedStates.add(
                                    GC_COUNT_CHECKER.hasOutlier(
                                            stat.stream().mapToDouble(ServerStat::getOneMinuteGCCount).toArray()
                                    )
                            );

                            // Выбираем самое опасное состояние.
                            state.setState(calculatedStates.stream().min(
                                    (s1, s2) -> IntegerType.compare(s1.ordinal(), s2.ordinal())
                            ).orElse(ServerState.State.GREEN));
                        })
        ).toArray(CompletableFuture[]::new))).thenCompose(
                aVoid -> serverStatStorage.writeStates(ctx.states.values())
        );
    }

    private static class Context {

        Map<String, ServerState> states;

        List<ServerName> names;
    }

}
