package ru.sber.hackaton.relocator.chief;

/**
 * Имя существующего сервера.
 */
public class ServerName {

    public ServerName() {
    }

    ServerName(String name) {
        id = name;
    }

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
