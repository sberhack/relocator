package ru.sber.hackaton.relocator.chief;

import java.util.Arrays;

/**
 * Проверка на выбросы.
 */
class OutlierChecker {

    /**
     * Порог проверки значения, скажем если значения маленькие, то и проверка на выбросы не нужна.
     */
    private final double threshold;

     OutlierChecker(double threshold) {
        this.threshold = threshold;
    }

    ServerState.State hasOutlier(double[] data) {
        if(data.length < 10) {
            return ServerState.State.GREEN;
        }

        ServerState.State res = ServerState.State.GREEN;

        double[] sortedArray = Arrays.copyOf(data, data.length);
        Arrays.sort(sortedArray);

        // Определение что выбросы существуют.
        // Если линия гладкая то не ну нужно искать выбросы.

        int index70 = data.length - (int)Math.floor(data.length / 100.0 * 20.0);
        double mean70 = 0.0;
        for(int i = 0; i < index70; i++) {
            mean70 += sortedArray[i];
        }
        mean70 = mean70 / index70;

        double mean20 = 0.0;
        for(int i = index70; i < data.length; i++) {
            mean20 += sortedArray[i];
        }
        mean20 = mean20 / (data.length - index70);

        if(mean20 / mean70 < 2.0) {
            // Более менее гладкая.
            return res;
        }

        // Проверка последних значений величину выброса.

        // > 45% - YELLOW
        int yellowIndex = data.length - (int)Math.floor(data.length / 100.0 * 45.0);
        if(checkLast(data, sortedArray[yellowIndex])) {
            res = ServerState.State.YELLOW;
        }

        // > 30% - ORANGE
        int orangeIndex = data.length - (int)Math.floor(data.length / 100.0 * 30.0);
        if(checkLast(data, sortedArray[orangeIndex])) {
            res = ServerState.State.ORANGE;
        }

        // > 15% - RED
        int redIndex = data.length - (int)Math.floor(data.length / 100.0 * 15.0);
        if(checkLast(data, sortedArray[redIndex])) {
            res = ServerState.State.RED;
        }

        return res;
    }

    private boolean checkLast(double[] data, double val) {
        if(data[data.length -1] > val && data[data.length -1] > threshold) {
            return true;
        }

        if(data.length > 3) {
            double threeMean = (data[data.length - 1] + data[data.length - 2]) / 2.0;

            return threeMean > val && threeMean > threshold;
        }

        return false;
    }

}
