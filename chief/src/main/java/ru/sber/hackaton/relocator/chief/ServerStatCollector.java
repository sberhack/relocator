package ru.sber.hackaton.relocator.chief;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sber.hackaton.relocator.toolkit.zookeeper.ZkClient;
import ru.sber.hackaton.relocator.transport.internal.Paths;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Сбор текущей статистики и сохранение ее в базе данных.
 */
@SuppressWarnings("unused")
@Singleton
public class ServerStatCollector {

    @Inject
    private ZkClient zkClient;

    @Inject
    private ServerStatStorage serverStatStorage;

    private static final Logger LOG = LoggerFactory.getLogger(ServerStatCollector.class);

    @Schedule(second = "*/5", minute = "*", hour = "*", persistent = false)
    public void collect()
            throws InterruptedException {
        final Map<String, List<ServerStatStorage.Stat>> serverStats = new HashMap<>();

        // Получаем список всех активных серверов на всех узлах.
        final Map<String, List<String>> activeServers = Paths.getActiveServers(zkClient);
        for (Map.Entry<String, List<String>> serverEntry : activeServers.entrySet()) {

            // Считаем количество подключенных клиентов к каждому узлу.
            int clientAmount = 0;
            final Map<String, Map<String, List<String>>> clientsOnNode =
                    Paths.getClientsConnectedToSpecifiedNode(zkClient, serverEntry.getKey());
            for (Map<String, List<String>> cl1 : clientsOnNode.values()) {
                for (List<String> cl2 : cl1.values()) {
                    clientAmount += cl2.size();
                }
            }
            final int clientAmountToSave = clientAmount;

            // Считываем текущую статистику по каждому серверу.
            final List<ServerStatStorage.Stat> serverRecords = serverEntry.getValue().stream().map(serverId -> {
                try {
                    return new ServerStatStorage.Stat(
                            zkClient.read(Paths.getServerPathOnNode(serverEntry.getKey(), serverId)),
                            clientAmountToSave
                    );
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();

                    return null;
                }
            }).filter(Objects::nonNull).collect(Collectors.toList());

            // Сохраняем полученную статистику в базе данных.
            serverStats.put(serverEntry.getKey(), serverRecords);
        }

        serverStatStorage.save(serverStats);
    }

}
