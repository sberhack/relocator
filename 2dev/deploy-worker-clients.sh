#!/bin/bash

cd ../worker-client-module || exit 1
mvn wildfly:deploy -Dwildfly.port=9991 -DclientModuleId=worker-client01
mvn wildfly:deploy -Dwildfly.port=9991 -DclientModuleId=worker-client02
mvn wildfly:deploy -Dwildfly.port=9991 -DclientModuleId=worker-client03
