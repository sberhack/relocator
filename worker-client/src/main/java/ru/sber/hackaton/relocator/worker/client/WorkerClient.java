package ru.sber.hackaton.relocator.worker.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sber.hackaton.relocator.transport.ClientRegistry;
import ru.sber.hackaton.relocator.worker.server.api.Worker;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;
import java.text.MessageFormat;
import java.time.Instant;

/**
 * Тестовый клиент, которые обращается к серверу с адресом "server".
 */
@SuppressWarnings("unused")
@Startup
@Singleton
public class WorkerClient {

    @Resource
    private TimerService timerService;

    @Resource(name = "java:app/env/clientModuleId")
    private String clientModuleId;

    @Inject
    private ClientRegistry clientRegistry;

    private Worker worker;

    /**
     * Интервал отправки запроса.
     */
    private static final int INTERVAL = 1000;

    private static final TimerConfig TIMER_CONFIG = new TimerConfig(null, false);

    private static final Logger LOG = LoggerFactory.getLogger(WorkerClient.class);

    @PostConstruct
    public void init() {
        // Получение прокси к удаленному серверу.
        try {
            worker = clientRegistry.get(clientModuleId, "server", Worker.class);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();

            return;
        }

        timerService.createSingleActionTimer(INTERVAL, TIMER_CONFIG);
    }

    @PreDestroy
    public void destroy() {
        for (Timer timer : timerService.getTimers()) {
            timer.cancel();
        }
    }

    @Timeout
    public void heartbeat() {
        final Instant startTime = Instant.now();
        final String request = clientModuleId + "<>" + Instant.now().toString();

        try {
            worker.callService(request)
                    .handle((r, e) -> {
                        try {
                            timerService.createSingleActionTimer(INTERVAL, TIMER_CONFIG);

                            if (e != null) {
                                LOG.error(MessageFormat.format(
                                        "Failed, moduleId: {0}", clientModuleId), e);
                            } else {
                                LOG.info("Success, moduleId: {}, response: {}", clientModuleId, r);
                            }

                        } catch (Throwable ex) {
                            LOG.error("Error", e);
                        }

                        return 0;
                    });

        } catch (Exception e) {
            LOG.error("Call service error", e);

            timerService.createSingleActionTimer(INTERVAL, TIMER_CONFIG);
        }

    }

}
