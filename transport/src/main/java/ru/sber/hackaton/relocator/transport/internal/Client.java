package ru.sber.hackaton.relocator.transport.internal;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalCause;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import ru.sber.hackaton.relocator.transport.ServerException;
import ru.sber.hackaton.relocator.transport.ShutdownException;
import ru.sber.hackaton.relocator.transport.TimeoutException;

import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.concurrent.ManagedThreadFactory;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Отправитель запросов к серверу, реализующему API.
 */
public final class Client implements Runnable {

    /**
     * ID заголовока с топиком, куда нужно отправлять ответы на запрос.
     */
    static final String REPLY_TO_HEADER = "replyTo";

    /**
     * Идентификатор модуля клиента.
     */
    private final String clientModuleId;

    /**
     * Идентификатор модуля сервера.
     */
    private final String serverModuleId;

    /**
     * API, которое доступно через межмодульное взаимодействие.
     */
    private final Class<?> apiInterface;

    /**
     * API, которое доступно через межмодульное взаимодействие (имя).
     */
    private final String apiInterfaceName;

    /**
     * Диспетчер отправки сообщений.
     */
    private final Dispatcher dispatcher;

    /**
     * Настройки транспорта.
     */
    private final Configuration configuration;

    /**
     * Заранее заготовленные заголовки, которые отправляются с сообщением.
     */
    private final List<Header> requestHeaders;

    /**
     * Kafka producer, отправляющий сообщения в очередь сервера.
     */
    private final Producer<String, Request> producer;

    /**
     * Kafka consumer очереди ответов сервера.
     */
    private final Consumer<String, Response> consumer;

    /**
     * Активные запросы, ожидающие ответа.
     */
    private final Cache<String, CompletableFuture<Object>> requests;

    /**
     * Поток, который обрабатывает очередь ответов.
     */
    private final Thread pollThread;

    /**
     * Флаг, что клиент активен.
     */
    private final AtomicBoolean running = new AtomicBoolean(true);

    private static final Logger LOG = LoggerFactory.getLogger(Server.class);

    public Client(
            String clientModuleId,
            String serverModuleId,
            Class<?> apiInterface,
            Dispatcher dispatcher,
            Configuration configuration,
            ManagedThreadFactory threadFactory,
            ManagedExecutorService executorService
    ) throws InterruptedException {
        this.clientModuleId = clientModuleId;
        this.serverModuleId = serverModuleId;
        this.apiInterface = apiInterface;
        this.apiInterfaceName = apiInterface.getName();
        this.dispatcher = dispatcher;
        this.configuration = configuration;

        // Получение топика, в который сервер будет направлять ответы на запросы.
        final String responsesTopic = dispatcher.getResponsesTopic(this);

        // Заголовки, отправляемые с запросом.
        requestHeaders = Collections.singletonList(new RecordHeader(
                REPLY_TO_HEADER, responsesTopic.getBytes(StandardCharsets.UTF_8)
        ));

        try {
            // Инициализация приемника ответов.
            consumer = new KafkaConsumer<>(configuration.getConsumerProperties());
            consumer.subscribe(Collections.singletonList(responsesTopic));

            // Инициализация очереди запросов к серверу.
            producer = new KafkaProducer<>(configuration.getProducerProperties());

            // Кеш для клиентский запросов.
            requests = Caffeine.newBuilder()
                    .executor(executorService)
                    .maximumSize(configuration.getMaxRequestsAmount())
                    .expireAfterWrite(configuration.getRequestTimeoutInMinutes(), TimeUnit.MINUTES)
                    .removalListener(this::requestRemovalListener)
                    .build();

            // Поток опроса очереди ответов от сервера.
            pollThread = threadFactory.newThread(this);

            dispatcher.registerClient(this);
        } catch (Exception e) {
            try {
                gracefullyClose();
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            } catch (Exception ex) {
                e.addSuppressed(ex);
            }

            throw e;
        }

        // Запуск опроса очереди ответов.
        pollThread.start();
    }

    String getClientModuleId() {
        return clientModuleId;
    }

    String getServerModuleId() {
        return serverModuleId;
    }

    Class<?> getApiInterface() {
        return apiInterface;
    }

    public CompletableFuture<Object> send(String methodName, Object[] args) {
        final String key = UUID.randomUUID().toString();
        final CompletableFuture<Object> future = new CompletableFuture<>();

        requests.put(key, future);

        try {
            producer.send(new ProducerRecord<>(
                    dispatcher.getRequestsTopic(this),
                    null,
                    key,
                    new Request(apiInterfaceName, methodName, args != null ? args : new Object[]{}),
                    requestHeaders
            ));
        } catch (Exception e) {
            requests.invalidate(key);

            future.completeExceptionally(e);
        }

        return future;
    }

    /**
     * Получение ответов сервера.
     */
    @Override
    public void run() {
        final Duration duration = Duration.ofSeconds(
                configuration.pollDurationInSeconds(), 0
        );

        while (running.get()) {
            try {
                final List<ConsumerRecord<String, Response>> recordsToProcess =
                        StreamSupport.stream(consumer.poll(duration).spliterator(), false)
                                .collect(Collectors.toList());
                if (recordsToProcess.isEmpty()) {
                    continue;
                }

                // TODO По хорошему нужно запускать через Managed Executor.
                recordsToProcess.parallelStream().forEach(this::processRecord);

                consumer.commitSync();
            } catch (WakeupException e) {
                // Check shutdown flag.
            } catch (Throwable e) {
                LOG.error("Poll or commit failed", e);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return clientModuleId.equals(client.clientModuleId) &&
                serverModuleId.equals(client.serverModuleId) &&
                apiInterface.equals(client.apiInterface);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientModuleId, serverModuleId, apiInterface);
    }

    public void close()
            throws InterruptedException {
        running.set(false);

        try {
            consumer.wakeup();
        } finally {
            try {
                pollThread.join();
            } finally {
                requests.asMap().values().forEach(
                        x -> x.completeExceptionally(new ShutdownException())
                );

                gracefullyClose();
            }
        }
    }

    private void processRecord(ConsumerRecord<String, Response> record) {
        MDC.put("cId", record.key());

        try {
            final CompletableFuture<Object> future = requests.getIfPresent(record.key());
            if (future != null) {
                requests.invalidate(record.key());

                if (record.value().getError() != null) {
                    future.completeExceptionally(new ServerException(record.value().getError()));
                } else {
                    future.complete(record.value().getResult());
                }
            }
        } finally {
            MDC.remove("cId");
        }
    }

    private void requestRemovalListener(String key, CompletableFuture<?> value, RemovalCause cause) {
        if (cause == RemovalCause.EXPIRED || cause == RemovalCause.SIZE) {
            if (value != null) {
                // Время ожидания ответа вышло.
                value.completeExceptionally(new TimeoutException());
            }
        }
    }

    private void gracefullyClose()
            throws InterruptedException {
        try {
            if (consumer != null) {
                consumer.close();
            }
        } finally {
            try {
                if (producer != null) {
                    producer.close();
                }

            } finally {
                dispatcher.unRegisterClient(this);
            }
        }
    }

}
