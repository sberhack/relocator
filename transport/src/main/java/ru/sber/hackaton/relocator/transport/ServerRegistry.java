package ru.sber.hackaton.relocator.transport;

import ru.sber.hackaton.relocator.transport.internal.Configuration;
import ru.sber.hackaton.relocator.transport.internal.Dispatcher;
import ru.sber.hackaton.relocator.transport.internal.Server;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedThreadFactory;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Регистрация реализации API, доступного через межмодульное взаимодействие.
 */
@ApplicationScoped
public class ServerRegistry {

    @Inject
    private Dispatcher dispatcher;

    @Inject
    private Configuration configuration;

    @Resource
    private ManagedThreadFactory threadFactory;

    private final Map<Object, Server> servers = new HashMap<>();

    private boolean running = true;

    private final Object sync = new Object();

    /**
     * Регистрация сервера, доступного через межмодульное вза
     *
     * @param serverModuleId Идентификатор модуля сервера.
     * @param server         Сервер, обрабатывающий удаленные вызовы.
     * @throws InterruptedException Выполнение потока прервано.
     */
    public void register(String serverModuleId, Object server)
            throws InterruptedException {
        final List<Class<?>> apiInterfaces = Arrays.stream(server.getClass().getInterfaces())
                .filter(i -> i.getAnnotation(Api.class) != null)
                .collect(Collectors.toList());

        final Server prev;

        synchronized (sync) {
            if (!running) {
                throw new IllegalStateException("stopped");
            }

            // Регистрация сведений о сервере, доступном через межмодульное взаимодействие.
            prev = servers.put(
                    server,
                    new Server(
                            serverModuleId,
                            server,
                            apiInterfaces,
                            dispatcher,
                            configuration,
                            threadFactory
                    )
            );
        }

        if (prev != null) {
            prev.close();
        }
    }

    /**
     * Удаление сервера, который теперь не доступн через межмодульное взаимодействие.
     *
     * @param server Удаляемый сервер.
     */
    public void remove(Object server) {
        final Server prev;
        synchronized (sync) {
            prev = servers.remove(server);
        }

        if (prev != null) {
            try {
                prev.close();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @SuppressWarnings("unused")
    @PreDestroy
    public void destroy() {
        final List<Server> serversToClose;

        synchronized (sync) {
            running = false;

            serversToClose = new ArrayList<>(servers.values());
        }

        boolean interrupted = false;

        for (Server server : serversToClose) {
            try {
                server.close();
            } catch (InterruptedException e) {
                interrupted = true;
            }
        }

        if (interrupted) {
            Thread.currentThread().interrupt();
        }
    }

}
