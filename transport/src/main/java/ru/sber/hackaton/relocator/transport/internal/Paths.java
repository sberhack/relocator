package ru.sber.hackaton.relocator.transport.internal;

import ru.sber.hackaton.relocator.toolkit.zookeeper.ZkClient;
import ru.sber.hackaton.relocator.toolkit.zookeeper.ZkPath;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Пути в Zookeeper.
 */
public final class Paths {

    private Paths() {
    }

    public static final String ROOT_NODE = "/relocator";

    private static final String SERVERS_NODE = "servers";

    private static final String CLIENTS_NODE = "clients";

    private static final String CLIENT_CONNECTION_NODE = "client_connections";

    /**
     * Извлекает ID зоны из ID узла.
     *
     * @param nodeId ID узла.
     * @return ID зоны, полученный из ID узла.
     */
    public static int getZoneId(String nodeId) {
        final String[] parts = nodeId.split("-");

        return Integer.parseInt(parts[0]);
    }

    /**
     * Путь к серверу на конкретном узле.
     *
     * @param nodeId   Узел, на котром расположен сервера.
     * @param serverId Идентификатор сервера (идентификатор модуля сервера + имя API, которое реализует сервер).
     * @return Путь к серверу на конкретном узле.
     */
    public static String getServerPathOnNode(String nodeId, String serverId) {
        return ZkPath.concat(
                ROOT_NODE,
                nodeId,
                SERVERS_NODE,
                serverId
        );
    }

    /**
     * Путь к серверу на конкретном узле.
     *
     * @param nodeId         Узел, на котром расположен сервера.
     * @param serverModuleId Идентификатор модуля сервера.
     * @param apiInterface   Имя API, которое реализует сервер.
     * @return Путь к серверу на конкретном узле.
     */
    static String getServerPathOnNode(String nodeId, String serverModuleId, String apiInterface) {
        return ZkPath.concat(
                ROOT_NODE,
                nodeId,
                SERVERS_NODE,
                formatServerId(serverModuleId, apiInterface)
        );
    }

    /**
     * Путь к клиенту на конкретном узле.
     *
     * @param nodeId         Узел, на котором расположен клиент.
     * @param serverId       Идентификатор сервера (идентификатор модуля сервера + имя API, которое реализует сервер).
     * @param clientModuleId Идентификатор модуля клиента.
     * @return Путь к клиенту на конкретном узле.
     */
    public static String getClientPathOnNode(
            String nodeId,
            String serverId,
            String clientModuleId
    ) {
        return ZkPath.concat(
                ROOT_NODE,
                nodeId,
                Paths.CLIENTS_NODE,
                clientModuleId,
                serverId
        );
    }

    /**
     * Путь к клиенту на конкретном узле.
     *
     * @param nodeId         Узел, на котором расположен клиент.
     * @param serverModuleId Имя модуля сервера, к которому обращается клиент.
     * @param apiInterface   Имя API, реализуемым сервером.
     * @param clientModuleId Идентификатор модуля клиента.
     * @return Путь к клиенту на конкретном узле.
     */
    static String getClientPathOnNode(
            String nodeId,
            String serverModuleId,
            String apiInterface,
            String clientModuleId
    ) {
        return ZkPath.concat(
                ROOT_NODE,
                nodeId,
                Paths.CLIENTS_NODE,
                clientModuleId,
                formatServerId(serverModuleId, apiInterface)
        );
    }

    /**
     * Путь к узлу клиента, который показзывает, к какому конкретно серверу подключен клиент.
     *
     * @param serverNodeId   Идентификатор узла сервера.
     * @param serverModuleId Идентификатор модуля сервера.
     * @param apiInterface   Имя API, реализуемым сервером.
     * @param clientNodeId   Идентификатор узла, на котором расположен клиент.
     * @param clientModuleId Идентификатор модуля клиентта.
     * @return Путь к узлу клиента, который показзывает, к какому конкретно серверу подключен клиент.
     */
    static String getClientConnectionPathOnNode(
            String serverNodeId,
            String serverModuleId,
            String apiInterface,
            String clientNodeId,
            String clientModuleId
    ) {
        return ZkPath.concat(
                ROOT_NODE,
                serverNodeId,
                Paths.CLIENT_CONNECTION_NODE,
                clientNodeId,
                clientModuleId,
                formatServerId(serverModuleId, apiInterface)
        );
    }

    /**
     * Возвращает список узлов, на которых расположены серверы, реализующие заданный API.
     *
     * @param zkClient       Zookeeper клиент.
     * @param serverModuleId Идентификатор модуля сервера.
     * @param apiInterface   Имя API, реализуемым сервером.
     * @return Список узлов, на которых расположены серверы, реализующие заданный API.
     * @throws InterruptedException Поток прерывается.
     */
    static List<String> getNodesWithSpecifiedServer(
            ZkClient zkClient,
            String serverModuleId,
            String apiInterface
    ) throws InterruptedException {
        final String serverId = formatServerId(serverModuleId, apiInterface);

        return zkClient.getSubPaths(ROOT_NODE).stream().filter(nodeId -> {
            try {
                return zkClient.getSubPaths(ZkPath.concat(ROOT_NODE, nodeId, SERVERS_NODE)).contains(serverId);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();

                return false;
            }
        }).collect(Collectors.toList());
    }

    /**
     * Возвращает список активных серверов на каждом узле (узел сервера - идентификаторы серверов на этом узле).
     *
     * @param zkClient Zookeeper клиент.
     * @return Список активных серверов на каждом узле.
     * @throws InterruptedException Поток прерывается.
     */
    public static Map<String, List<String>> getActiveServers(ZkClient zkClient)
            throws InterruptedException {
        final Map<String, List<String>> result = new HashMap<>();

        final List<String> nodeIds = zkClient.getSubPaths(ROOT_NODE);
        for (String nodeId : nodeIds) {
            result.put(nodeId, zkClient.getSubPaths(ZkPath.concat(ROOT_NODE, nodeId, SERVERS_NODE)));
        }

        return result;
    }

    /**
     * Возвращает список активных клиентов на каждом узле
     * (узел клиента - идентификаторы модулей клиентов - идентфикаторы серверов).
     *
     * @param zkClient Zookeeper клиент.
     * @return Список активных серверов на каждом узле.
     * @throws InterruptedException Поток прерывается.
     */
    public static Map<String, Map<String, List<String>>> getActiveClients(ZkClient zkClient)
            throws InterruptedException {
        final Map<String, Map<String, List<String>>> result = new HashMap<>();

        final List<String> nodeIds = zkClient.getSubPaths(ROOT_NODE);
        for (String nodeId : nodeIds) {
            final Map<String, List<String>> clientServerIds = new HashMap<>();

            final String clientModuleIdsNode = ZkPath.concat(ROOT_NODE, nodeId, CLIENTS_NODE);
            final List<String> clientModuleIds = zkClient.getSubPaths(clientModuleIdsNode);

            for (String clientModuleId : clientModuleIds) {
                clientServerIds.put(
                        clientModuleId,
                        zkClient.getSubPaths(ZkPath.concat(clientModuleIdsNode, clientModuleId))
                );
            }

            result.put(nodeId, clientServerIds);
        }

        return result;
    }

    /**
     * Возвращает список клиентов, которые подключены к серверам на определенном узле.
     * (идентификатор модуля клиентоа - идентфикаторы серверов).
     *
     * @param zkClient Zookeeper клиент.
     * @param nodeId   Узел, на котором находятся серверы, к которым подключены клиенты.
     * @return Список клиентов, которые подключены к серверам на определенном узле.
     * @throws InterruptedException Поток прерывается.
     */
    public static Map<String, Map<String, List<String>>> getClientsConnectedToSpecifiedNode(
            ZkClient zkClient,
            String nodeId
    ) throws InterruptedException {
        final Map<String, Map<String, List<String>>> result = new HashMap<>();

        final String clientConnectionNode = ZkPath.concat(ROOT_NODE, nodeId, CLIENT_CONNECTION_NODE);
        final List<String> clientNodeIds = zkClient.getSubPaths(clientConnectionNode);

        for (String clientNodeId : clientNodeIds) {
            final Map<String, List<String>> clientModules = new HashMap<>();

            final String clientNodeIdNode = ZkPath.concat(clientConnectionNode, clientNodeId);
            final List<String> clientModuleIds = zkClient.getSubPaths(clientNodeIdNode);

            for (String clientModuleId : clientModuleIds) {
                clientModules.put(
                        clientModuleId,
                        zkClient.getSubPaths(ZkPath.concat(clientNodeIdNode, clientModuleId))
                );
            }

            result.put(clientNodeId, clientModules);
        }

        return result;
    }

    private static String formatServerId(String serverModuleId, String apiInterface) {
        return serverModuleId + "-" + apiInterface;
    }

}
