package ru.sber.hackaton.relocator.transport.internal;

import com.codahale.metrics.Timer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import javax.enterprise.concurrent.ManagedThreadFactory;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Приемник запросов к серверу, реализующему API.
 */
public final class Server implements Runnable {

    /**
     * Идентификатор модуля сервера.
     */
    private final String serverModuleId;

    /**
     * Список API, реализуемых сервером.
     */
    private final List<Class<?>> apiInterfaces;

    /**
     * Объект, который реализует API (он будет обрабатывать запросы клиентов).
     */
    private final Object server;

    /**
     * Диспетчер транспорта.
     */
    private final Dispatcher dispatcher;

    /**
     * Конфигурация транспорта.
     */
    private final Configuration configuration;

    /**
     * Приемник сообщений - запросов к серверу.
     */
    private final Consumer<String, Request> consumer;

    /**
     * Отправитель ответов клиентам.
     */
    private final Producer<String, Response> producer;

    /**
     * Поток приема сообщений - запросов.
     */
    private final Thread pollThread;

    /**
     * Флаг, что сервер готов принимать сообщения.
     */
    private final AtomicBoolean running = new AtomicBoolean(true);

    /**
     * Кеш методов, которые вызываются при обработке запросов.
     */
    private static final ConcurrentHashMap<String, MethodHandle> METHOD_HANDLE_CACHE =
            new ConcurrentHashMap<>();

    private static final Logger LOG = LoggerFactory.getLogger(Server.class);

    public Server(
            String serverModuleId,
            Object server,
            List<Class<?>> apiInterfaces,
            Dispatcher dispatcher,
            Configuration configuration,
            ManagedThreadFactory threadFactory
    ) throws InterruptedException {
        this.serverModuleId = serverModuleId;
        this.apiInterfaces = apiInterfaces;
        this.server = server;
        this.dispatcher = dispatcher;
        this.configuration = configuration;

        try {
            // Настройка приемника запросов от клиентов.
            consumer = new KafkaConsumer<>(configuration.getConsumerProperties());
            consumer.subscribe(dispatcher.getRequestsTopics(this));

            // Настройка отправителя ответов.
            producer = new KafkaProducer<>(configuration.getProducerProperties());

            // Поток обработки запросов.
            pollThread = threadFactory.newThread(this);

            dispatcher.registerServer(this);
        } catch (Exception e) {
            // Что то пошло не так, корректно завершаемся.
            try {
                gracefullyClose();
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            } catch (Exception ex) {
                e.addSuppressed(ex);
            }
            throw e;
        }

        pollThread.start();
    }

    String getServerModuleId() {
        return serverModuleId;
    }

    List<Class<?>> getApiInterfaces() {
        return apiInterfaces;
    }

    public void close()
            throws InterruptedException {
        running.set(false);

        try {
            consumer.wakeup();
        } finally {
            try {
                pollThread.join();
            } finally {
                gracefullyClose();
            }
        }
    }

    /**
     * Получение запросов от клиента.
     */
    @Override
    public void run() {
        final Duration duration = Duration.ofSeconds(
                configuration.pollDurationInSeconds(), 0
        );

        while (running.get()) {
            try {
                final List<RecordToProcess> recordsToProcess =
                        StreamSupport.stream(consumer.poll(duration).spliterator(), false)
                                .map(x -> new RecordToProcess(
                                        dispatcher.markRequestOnServer(
                                                Server.this, x.value().getApiInterface()
                                        ),
                                        x
                                )).collect(Collectors.toList());
                if (recordsToProcess.isEmpty()) {
                    continue;
                }

                // TODO По хорошему нужно запускать через Managed Executor.
                recordsToProcess.parallelStream().forEach(this::processRecord);

                consumer.commitSync();
            } catch (WakeupException e) {
                // Check shutdown flag.
            } catch (Exception e) {
                LOG.error("Poll or commit failed", e);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Server server = (Server) o;
        return serverModuleId.equals(server.serverModuleId) &&
                apiInterfaces.equals(server.apiInterfaces);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serverModuleId, apiInterfaces);
    }

    private void processRecord(RecordToProcess x) {
        MDC.put("cId", x.record.key());

        try {
            try {
                final CompletableFuture<?> result =
                        (CompletableFuture<?>) getMethodHandle(x.record).invokeWithArguments(
                                Arrays.asList(x.record.value().getArguments())
                        );

                send(x.record, (Serializable) result.get(), null);
            } catch (Throwable e) {
                dispatcher.markRequestErrorOnServer(this, x.record.value().getApiInterface());

                LOG.error(MessageFormat.format("Message {0} process failed",
                        x.record.key()), e
                );

                send(x.record, null, e);
            } finally {
                x.callContext.stop();
            }
        } catch (Throwable e) {
            if(x.record.value() != null) {
                dispatcher.markRequestErrorOnServer(this, x.record.value().getApiInterface());
            }

            LOG.error(MessageFormat.format("Message {0} process fatal error",
                    x.record.key()), e
            );
        } finally {
            MDC.remove("cId");
        }
    }

    private void send(ConsumerRecord<String, Request> record, Serializable result, Throwable e) {
        // Имя топика, куда отправлять ответы, находится в заголовке запроса.
        final String replyTo = new String(
                record.headers().lastHeader(Client.REPLY_TO_HEADER).value(),
                StandardCharsets.UTF_8
        );

        final String errorMessage;
        if (e != null) {
            final StringWriter sw = new StringWriter();
            try (final PrintWriter pw = new PrintWriter(sw)) {
                e.printStackTrace(pw);
                pw.flush();
                sw.flush();
                errorMessage = sw.toString();
            }
        } else {
            errorMessage = null;
        }

        producer.send(new ProducerRecord<>(replyTo, record.key(), new Response(result, errorMessage)));
    }

    private MethodHandle getMethodHandle(ConsumerRecord<String, Request> record) {
        final String cacheKey = record.value().getMethod() + "_" +
                (record.value().getArguments() != null ?
                        Arrays.stream(record.value().getArguments())
                                .map(i -> i.getClass().getName())
                                .collect(Collectors.joining("-a-"))
                        : "");

        return METHOD_HANDLE_CACHE.computeIfAbsent(cacheKey, key -> {
            final MethodHandles.Lookup publicLookup = MethodHandles.publicLookup();

            // Каждый метод API должен возвращать CompletableFuture.
            final MethodType mt = MethodType.methodType(
                    CompletableFuture.class,
                    Arrays.stream(record.value().getArguments())
                            .map(Object::getClass)
                            .toArray(Class<?>[]::new)
            );

            try {
                return publicLookup.findVirtual(
                        server.getClass(), record.value().getMethod(), mt
                ).bindTo(server);
            } catch (NoSuchMethodException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private void gracefullyClose()
            throws InterruptedException {
        try {
            consumer.close();
        } finally {
            try {
                producer.close();
            } finally {
                dispatcher.unRegisterServer(this);
            }
        }
    }

    private static class RecordToProcess {

        final Timer.Context callContext;

        final ConsumerRecord<String, Request> record;

        RecordToProcess(Timer.Context callContext, ConsumerRecord<String, Request> record) {
            this.callContext = callContext;
            this.record = record;
        }

    }

}
