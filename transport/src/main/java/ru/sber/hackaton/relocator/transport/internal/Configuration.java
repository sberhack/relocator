package ru.sber.hackaton.relocator.transport.internal;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import ru.sber.hackaton.relocator.toolkit.kafka.SerializableDeserializer;
import ru.sber.hackaton.relocator.toolkit.kafka.SerializableSerializer;

import javax.enterprise.context.ApplicationScoped;
import java.util.Properties;

/**
 * Найтроки доступа к Kafka.
 */
@ApplicationScoped
public class Configuration {

    private static final String KAFKA_CONNECT = "KAFKA_CONNECT";

    /**
     * @return Настройки приемников сообщений Kafka.
     */
    Properties getConsumerProperties() {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                System.getenv(KAFKA_CONNECT));
        props.put(ConsumerConfig.GROUP_ID_CONFIG,
                "ru.sber.hackaton.relocator");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                SerializableDeserializer.class.getName());

        return props;
    }

    /**
     * @return Настройки источников сообщений Kafka.
     */
    Properties getProducerProperties() {
        final Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                System.getenv(KAFKA_CONNECT));
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                SerializableSerializer.class.getName());

        return props;
    }

    /**
     * @return Время ожидания приема новых сообщений Kafka.
     */
    long pollDurationInSeconds() {
        // TODO Задать настройкой.

        return 1;
    }

    /**
     * @return Максимальное число запросов к серверу на клиенте.
     */
    long getMaxRequestsAmount() {
        // TODO Задать настройкой.

        return 10_000;
    }

    /**
     * @return Максимальное время ожидания ответа сервера.
     */
    long getRequestTimeoutInMinutes() {
        // TODO Задать настройкой.

        return 1;
    }

    /**
     * @return Период сохранения текущей статистики в Zookeeper.
     */
    long heartbeatRateInMilliseconds() {
        // TODO Задать настройкой.

        return 500;
    }

}
