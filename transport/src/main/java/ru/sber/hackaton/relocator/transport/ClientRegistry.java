package ru.sber.hackaton.relocator.transport;

import ru.sber.hackaton.relocator.transport.internal.Client;
import ru.sber.hackaton.relocator.transport.internal.Configuration;
import ru.sber.hackaton.relocator.transport.internal.Dispatcher;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.concurrent.ManagedThreadFactory;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Получение прокси классов для использования API через межмодульное взаимодействие.
 */
@ApplicationScoped
public class ClientRegistry {

    @Inject
    private Dispatcher dispatcher;

    @Inject
    private Configuration configuration;

    @Resource
    private ManagedThreadFactory threadFactory;

    @Resource
    private ManagedExecutorService executorService;

    private final Map<String, Client> clients = new HashMap<>();

    private boolean running = true;

    private final Object sync = new Object();

    /**
     * Получение прокси для обращения к серверу через межмодульное взаимодействие.
     * @param clientModuleId Идентификатор модуля клиента.
     * @param serverModuleId Идентификатор модуля сервера.
     * @param serverApiClass API сервера.
     * @param <T> Тип API сервера.
     * @return API сервера, который доступн через межмодульное взаимодействие.
     * @throws InterruptedException Выполнение потока прервано.
     */
    @SuppressWarnings("unchecked")
    public <T> T get(String clientModuleId, String serverModuleId, Class<T> serverApiClass)
            throws InterruptedException {
        final Client client;

        synchronized (sync) {
            if (!running) {
                throw new IllegalStateException("stopped");
            }

            // Получение прокси к серверу.
            client = clients.computeIfAbsent(
                    clientModuleId + "-" + serverModuleId + "-" + serverApiClass.getName(),
                    key -> {
                        // Если прокси еще не было  - он создается, иначе возвращается уже созданный
                        // для связки ID модуля клиента, ID модуля сервера и имени интерфейса.
                        try {
                            return new Client(
                                    clientModuleId,
                                    serverModuleId,
                                    serverApiClass,
                                    dispatcher,
                                    configuration,
                                    threadFactory,
                                    executorService
                            );
                        } catch (InterruptedException e) {
                            return null;
                        }
                    }
            );
        }

        if (client == null) {
            throw new InterruptedException();
        }

        return (T) Proxy.newProxyInstance(
                Thread.currentThread().getContextClassLoader(),
                new Class[]{ serverApiClass },
                (proxy, method, args) -> {
                    switch (method.getName()) {
                        case "getClass":
                            return serverApiClass;
                        case "toString":
                            return serverApiClass.getName();
                        case "hashCode":
                            return 0;
                        case "equals":
                            return false;
                        case "notify":
                        case "notifyAll":
                        case "wait":
                            return null;
                        default:
                            return client.send(method.getName(), args);
                    }
                });
    }

    @SuppressWarnings("unused")
    @PreDestroy
    public void destroy() {
        final List<Client> clientsToClose;

        synchronized (sync) {
            running = false;

            clientsToClose = new ArrayList<>(clients.values());
        }

        boolean interrupted = false;

        // Закрываем все клиенты, сведения о том что ни живы удаляются из Zookeeper.
        for (Client client : clientsToClose) {
            try {
                client.close();
            } catch (InterruptedException e) {
                interrupted = true;
            }
        }

        if (interrupted) {
            Thread.currentThread().interrupt();
        }
    }

}
