package ru.sber.hackaton.relocator.transport;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Аннотация для отметки API, доступного через межмодульное взаимодействие.
 * Каждый метод API должен возвращать CompletableFuture.
 */
@Qualifier
@Retention(RUNTIME)
@Target({TYPE})
public @interface Api {
}
