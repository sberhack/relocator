package ru.sber.hackaton.relocator.transport.internal;

import java.io.Serializable;

/**
 * Контейнер ответа от сервера, реализующего API.
 */
final class Response implements Serializable {

    private static final long serialVersionUID = 5997964572638080526L;

    private final Serializable result;

    private final String error;

    Response(Serializable result, String error) {
        this.result = result;
        this.error = error;
    }

    Serializable getResult() {
        return result;
    }

    String getError() {
        return error;
    }

}
