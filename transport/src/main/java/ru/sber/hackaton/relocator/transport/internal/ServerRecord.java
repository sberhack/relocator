package ru.sber.hackaton.relocator.transport.internal;

import java.io.Serializable;
import java.time.Instant;

/**
 * Дата последнего сообщения об активном сервере.
 */
public class ServerRecord implements Serializable {

    private static final long serialVersionUID = 1687165134371149968L;

    private final long maxMemory;

    private final long usedMemory;

    private final double processorLoading;

    private final double oneMinuteRateRequest;

    private final double oneMinuteTimeRequest;

    private final double oneMinuteRateError;

    private final double oneMinuteGCTime;

    private final double oneMinuteGCCount;

    private final Instant date = Instant.now();

    ServerRecord(
            long maxMemory,
            long usedMemory,
            double processorLoading,
            double oneMinuteRateRequest,
            double oneMinuteTimeRequest,
            double oneMinuteRateError,
            double oneMinuteGCTime,
            double oneMinuteGCCount

    ) {
        this.maxMemory = maxMemory;
        this.usedMemory = usedMemory;
        this.processorLoading = processorLoading;
        this.oneMinuteRateRequest = oneMinuteRateRequest;
        this.oneMinuteTimeRequest = oneMinuteTimeRequest;
        this.oneMinuteRateError = oneMinuteRateError;
        this.oneMinuteGCTime = oneMinuteGCTime;
        this.oneMinuteGCCount = oneMinuteGCCount;
    }

    public long getMaxMemory() {
        return maxMemory;
    }

    public long getUsedMemory() {
        return usedMemory;
    }

    public double getProcessorLoading() {
        return processorLoading;
    }

    public double getOneMinuteRateRequest() {
        return oneMinuteRateRequest;
    }

    public double getOneMinuteTimeRequest() {
        return oneMinuteTimeRequest;
    }

    public double getOneMinuteRateError() {
        return oneMinuteRateError;
    }

    public double getOneMinuteGCTime() {
        return oneMinuteGCTime;
    }

    public double getOneMinuteGCCount() {
        return oneMinuteGCCount;
    }

    public Instant getDate() {
        return date;
    }

}
