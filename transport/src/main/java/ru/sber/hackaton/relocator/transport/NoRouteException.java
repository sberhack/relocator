package ru.sber.hackaton.relocator.transport;

/**
 * К запрошенному серверу нет маршрута.
 */
public class NoRouteException extends RuntimeException {

    public NoRouteException() {
    }

    @SuppressWarnings("unused")
    public NoRouteException(String message) {
        super(message);
    }

    @SuppressWarnings("unused")
    public NoRouteException(String message, Throwable cause) {
        super(message, cause);
    }

    @SuppressWarnings("unused")
    public NoRouteException(Throwable cause) {
        super(cause);
    }

}
