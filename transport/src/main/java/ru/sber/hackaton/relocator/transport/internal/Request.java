package ru.sber.hackaton.relocator.transport.internal;

import java.io.Serializable;

/**
 * Контейнер запроса к методы API.
 */
final class Request implements Serializable {

    private static final long serialVersionUID = 5582820430824074357L;

    private final String apiInterface;

    private final String method;

    private final Object[] arguments;

    Request(String apiInterface, String method, Object[] arguments) {
        this.apiInterface = apiInterface;
        this.method = method;
        this.arguments = arguments;
    }

    String getApiInterface() {
        return apiInterface;
    }

    String getMethod() {
        return method;
    }

    Object[] getArguments() {
        return arguments;
    }

}
