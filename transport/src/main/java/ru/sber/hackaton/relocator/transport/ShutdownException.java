package ru.sber.hackaton.relocator.transport;

/**
 * Программа завершается.
 */
public class ShutdownException extends RuntimeException {

    public ShutdownException() {
    }

    @SuppressWarnings("unused")
    public ShutdownException(String message) {
        super(message);
    }

    @SuppressWarnings("unused")
    public ShutdownException(String message, Throwable cause) {
        super(message, cause);
    }

    @SuppressWarnings("unused")
    public ShutdownException(Throwable cause) {
        super(cause);
    }

}
