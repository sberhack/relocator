package ru.sber.hackaton.relocator.transport;

/**
 * Истекло время ожидания ответа.
 */
public class TimeoutException extends RuntimeException {

    public TimeoutException() {
    }

    @SuppressWarnings("unused")
    public TimeoutException(String message) {
        super(message);
    }

    @SuppressWarnings("unused")
    public TimeoutException(String message, Throwable cause) {
        super(message, cause);
    }

    @SuppressWarnings("unused")
    public TimeoutException(Throwable cause) {
        super(cause);
    }

}
