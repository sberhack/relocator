package ru.sber.hackaton.relocator.transport.internal;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SlidingTimeWindowArrayReservoir;
import com.codahale.metrics.Timer;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sber.hackaton.relocator.toolkit.zookeeper.ZkClient;
import ru.sber.hackaton.relocator.transport.NoRouteException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.Serializable;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.OperatingSystemMXBean;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Диспетчер транспорта.
 */
@ApplicationScoped
public class Dispatcher {

    /**
     * Клиент к Zookeeper (в нем хранится текуще состояние кластера).
     */
    @Inject
    private ZkClient zkClient;

    /**
     * Конфигурация транспорта.
     */
    @Inject
    private Configuration configuration;

    /**
     * Сервис для запуска задач по расписанию.
     */
    @Resource
    private ManagedScheduledExecutorService scheduledExecutorService;

    /**
     * Хранилище метрик транспорта.
     */
    private final MetricRegistry metricRegistry = new MetricRegistry();

    /**
     * Карта активных клентов.
     */
    private final Map<Client, ClientInfo> activeClients = new ConcurrentHashMap<>();

    /**
     * Карта активных серверов (значение - карта API интерфейсов, которые реализует сервис).
     */
    private final Map<Server, Map<String, ServerInfo>> activeServers = new ConcurrentHashMap<>();

    /**
     * Периодическое сохранение текущих метрик трансопрта в Zookeeper, а также обновление рекомендуемых настроек для клиентов.
     */
    private ScheduledFuture heartbeatSchedule;

    /**
     * Получение случайных чисел, чтобы клиент, при отсутствии рекомендаций, подключался к случайно выбранному серверу.
     */
    private final Random random = new Random(new Date().getTime());

    /**
     * Доступ к статистическим данным JVM по памяти.
     */
    private final MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();

    /**
     * Доступ к статистическим данным операционной системы.
     */
    private final OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();

    /**
     * Доступ к статистическим данным сборки мусора (1 поколение).
     */
    private final GarbageCollectorMXBean garbageCollectorMXBean = ManagementFactory.getGarbageCollectorMXBeans().get(0);

    /**
     * Предыдущее значение времени, потраченное на сборку мусора.
     */
    private long prevGcTime;

    /**
     * Предыдущее значение количества сборок мусора.
     */
    private long prevGcCount;

    /**
     * Имя метрики - кол-во запросов к серверу.
     */
    private static final String REQUEST_COUNTER = "requests";

    /**
     * Имя метрики - кол-во ошибок.
     */
    private static final String REQUEST_ERROR_COUNTER = "requestErrors";

    /**
     * Имя метрики - время отклика сервера.
     */
    private static final String REQUEST_TIMER = "request-timer";

    /**
     * Имя метрики - количество времени, потраченное на сбор мусора.
     */
    private static final String GC_TIME = "GC-time";

    /**
     * Имя метрики - количество сборов мусора.
     */
    private static final String GC_COUNT = "GC-count";

    private static final int TIME_WINDOW = 1;

    /**
     * Идентификатор узла, на котором размещен текущий модуль.
     */
    private static final String NODE_ID = System.getenv("NODE_ID");

    private static final Logger LOG = LoggerFactory.getLogger(Dispatcher.class);

    @SuppressWarnings("unused")
    @PostConstruct
    public void init() {
        // Запоминаем значение по сборке мусора.
        prevGcTime = garbageCollectorMXBean.getCollectionTime();
        prevGcCount = garbageCollectorMXBean.getCollectionCount();

        // Запускаем периодическую задачу - сохранение метрик в Zookeeper и обновление из Zookeeper
        // рекомендуемых серверах для клиентов (обновление происходит на всякий случай, вдруг не было получено
        // уведомление о смене рекомендации через Watcher).
        heartbeatSchedule = scheduledExecutorService.scheduleAtFixedRate(
                this::heartbeat,
                configuration.heartbeatRateInMilliseconds(),
                configuration.heartbeatRateInMilliseconds(),
                TimeUnit.MILLISECONDS
        );
    }

    @SuppressWarnings("unused")
    @PreDestroy
    public void destroy() {
        heartbeatSchedule.cancel(false);
    }

    void registerClient(Client client)
            throws InterruptedException {
        final ClientInfo clientInfo = new ClientInfo(client);

        LOG.info("Register client, zkPath: {}", clientInfo.aliveNodePath);

        // Регистрация в Zookeeper ветки активного клиента (в эфемерном узле, если клиент внезапно пропадет,
        // Zookeeper удалит узел самостоятельно)
        // и регистрация Watcher для оперативного получения
        // рекомендаций о сервере, на который отправлять запросы.
        zkClient.addEphemeralNode(client, clientInfo.aliveNodePath, clientInfo);

        activeClients.put(client, clientInfo);
    }

    void unRegisterClient(Client client)
            throws InterruptedException {
        final ClientInfo clientInfo = activeClients.remove(client);

        if (clientInfo != null) {
            LOG.info("Unregister client, zkPath: {}", clientInfo.aliveNodePath);

            // Удаление из Zookeeper ветки теперь не активного клиента.
            zkClient.removeNodeOwner(client);
        }
    }

    void registerServer(Server server)
            throws InterruptedException {
        final Map<String, ServerInfo> serverInfo = server.getApiInterfaces().stream().collect(Collectors.toMap(
                Class::getName,
                x -> {
                    final ServerInfo si = new ServerInfo(server, x);

                    LOG.info("Register server, zkPath: {}", si.aliveNodePath);

                    return si;
                }));

        // Регистрация в Zookeeper ветки активного сервера (ветка на каждый API, реализуемый сервером),
        // создается эфемерный узел, если сервер внезапно пропадет, Zookeeper удалит узел самостоятельно.
        zkClient.addEphemeralNodes(
                server,
                serverInfo.values().stream().map(x -> x.aliveNodePath).collect(Collectors.toList()),
                null
        );

        activeServers.put(server, serverInfo);
    }

    void unRegisterServer(Server server)
            throws InterruptedException {
        final Map<String, ServerInfo> serverInfo = activeServers.remove(server);

        if (serverInfo != null) {
            serverInfo.forEach((key, value) -> {
                LOG.info("Unregister server, zkPath: {}", value.aliveNodePath);

                // Удаление метрик по уже не активному серверу.
                metricRegistry.remove(
                        MetricRegistry.name(value.server.getServerModuleId(), key, REQUEST_COUNTER)
                );
                metricRegistry.remove(
                        MetricRegistry.name(value.server.getServerModuleId(), key, REQUEST_TIMER)
                );
            });

            // Удаление из Zookeeper ветки теперь не активного сервера.
            zkClient.removeNodeOwner(server);
        }
    }

    /**
     * Возвращает топик сервера, в который клиент будет класть запросы для конкретного сервера.
     *
     * @param client Клиент, который хочет отправить запрос серверу.
     * @return Топик конкретного сервера, который будет обрабатывать запрос или null, если такого сервера нет.
     * @throws InterruptedException Идет прерывание потока исполнения.
     */
    String getRequestsTopic(Client client)
            throws InterruptedException {
        return Objects.requireNonNull(activeClients.get(client)).getTopic();
    }

    /**
     * Возвращает топик, в который сервер будет класть ответы клиенту. Данные о нем передаются в заголовке запроса.
     *
     * @param client Клиент, который хочет отправлять запросы серверу.
     * @return Топик для ответов на запросы от клиента.
     */
    String getResponsesTopic(Client client) {
        return MessageFormat.format(
                "{0}-{1}-{2}-{3}",
                NODE_ID,
                client.getClientModuleId(),
                client.getServerModuleId(),
                client.getApiInterface().getName()
        );
    }

    /**
     * Возвращает список топиков, в которые клиенты будут отправлять запросы серверу (для каждого API свой топик).
     *
     * @param server Сервер, который принимает запросы от клиентов.
     * @return Список топиков для приема запросов.
     */
    List<String> getRequestsTopics(Server server) {
        return server.getApiInterfaces().stream()
                .map(x -> MessageFormat.format(
                        "{0}-{1}-{2}",
                        NODE_ID,
                        server.getServerModuleId(),
                        x.getName()
                ))
                .collect(Collectors.toList());
    }

    /**
     * Отметка сервера о том что пришел запрос от клиента.
     *
     * @param server       Сервер, который принимает запросы от клиентов.
     * @param apiInterface Имя API, который обрабатывает запрос.
     * @return Контекст вызова для отметки сервером времени, потраченного на обработку запроса.
     */
    Timer.Context markRequestOnServer(Server server, String apiInterface) {
        final Map<String, ServerInfo> serverInfo = activeServers.get(server);

        if (serverInfo != null) {
            metricRegistry.meter(MetricRegistry.name(
                    Objects.requireNonNull(serverInfo.get(apiInterface)).server.getServerModuleId(),
                    apiInterface,
                    REQUEST_COUNTER
            )).mark();

            return metricRegistry.timer(MetricRegistry.name(
                    Objects.requireNonNull(serverInfo.get(apiInterface)).server.getServerModuleId(),
                    apiInterface,
                    REQUEST_TIMER
            ), () -> new Timer(new SlidingTimeWindowArrayReservoir(TIME_WINDOW, TimeUnit.MINUTES))).time();
        }

        return null;
    }

    /**
     * Отметка сервера о том что пришел запрос от клиента.
     *
     * @param server       Сервер, который принимает запросы от клиентов.
     * @param apiInterface Имя API, который обрабатывает запрос.
     */
    void markRequestErrorOnServer(Server server, String apiInterface) {
        final Map<String, ServerInfo> serverInfo = activeServers.get(server);

        if (serverInfo != null) {
            metricRegistry.meter(MetricRegistry.name(
                    Objects.requireNonNull(serverInfo.get(apiInterface)).server.getServerModuleId(),
                    apiInterface,
                    REQUEST_ERROR_COUNTER
            )).mark();
        }
    }

    /**
     * Периодическое обновление из Zookeeper рекомендаций по подключению клиентов к конкретному серверу.
     * А также сохраненение в Zookeeper текущих метрик по каждому серверу.
     */
    @SuppressWarnings("unchecked")
    private void heartbeat() {
        for (ClientInfo clientInfo : activeClients.values().toArray(new ClientInfo[0])) {
            // Обновление рекомендованного сервера, если нужно.
            try {
                clientInfo.updateTopicIfNecessary();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();

                return;
            } catch (Exception e) {
                LOG.error(MessageFormat.format(
                        "Fail to read recommends for client, zkPath: {0}", clientInfo.aliveNodePath
                ), e);
            }
        }

        // Сохранение статистики по серверу.

        // Общие метрики JVM.
        final long memoryMax = memoryMXBean.getHeapMemoryUsage().getMax();
        final long memoryUsage = memoryMXBean.getHeapMemoryUsage().getUsed();
        final double loadAverage = operatingSystemMXBean.getSystemLoadAverage();

        // Для метрик по сбору мусора засекаем их изменение отностительно предыдущего значения,
        // поскольку это нарастающие метрики - нам нужно выяснить скорость их роста.
        final long gcTime = garbageCollectorMXBean.getCollectionTime();
        final long gcCounter = garbageCollectorMXBean.getCollectionCount();
        final Meter gcTimeMeter = metricRegistry.meter(GC_TIME);
        final Meter gcCounterMeter = metricRegistry.meter(GC_COUNT);

        gcTimeMeter.mark(gcTime - prevGcTime);
        gcCounterMeter.mark(gcCounter - prevGcCount);

        prevGcTime = gcTime;
        prevGcCount = gcCounter;

        final Map<String, Serializable> dataToSave = new HashMap<>();

        for (Map<String, ServerInfo> item : activeServers.values().toArray(new Map[0])) {
            for (Map.Entry<String, ServerInfo> entry : item.entrySet()) {
                // Метрики по запросам к серверу.
                final Meter requestCounter = metricRegistry.getMeters().get(MetricRegistry.name(
                        entry.getValue().server.getServerModuleId(),
                        entry.getKey(),
                        REQUEST_COUNTER
                ));
                final Timer requestTimer = metricRegistry.getTimers().get(MetricRegistry.name(
                        entry.getValue().server.getServerModuleId(),
                        entry.getKey(),
                        REQUEST_TIMER
                ));
                final Meter requestErrorCounter = metricRegistry.getMeters().get(MetricRegistry.name(
                        entry.getValue().server.getServerModuleId(),
                        entry.getKey(),
                        REQUEST_ERROR_COUNTER
                ));

                dataToSave.put(entry.getValue().aliveNodePath, new ServerRecord(
                        memoryMax,
                        memoryUsage,
                        loadAverage,
                        requestCounter != null ? requestCounter.getOneMinuteRate() : 0,
                        requestTimer != null ? Duration.of(
                                (long)requestTimer.getSnapshot().getMean(), ChronoUnit.NANOS
                        ).toMillis() : 0,
                        requestErrorCounter != null ? requestErrorCounter.getOneMinuteRate() : 0,
                        gcTimeMeter.getOneMinuteRate(),
                        gcCounterMeter.getOneMinuteRate()
                ));
            }
        }

        // Сохраняем статистику в Zookeeper, в эфемерных узлах (чтобы они автоматически удалялись, когда не нужны).
        try {
            zkClient.writeIfPathExists(dataToSave);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            LOG.error("Failed to send heartbeat", e);
        }
    }

    private class ClientInfo implements Watcher {

        private final Client client;

        /**
         * Zookeeper узел, который существует пока жив клиента.
         */
        private final String aliveNodePath;

        /**
         * Топик конкретного сервера, в который будет направляться запрос от клиента.
         * Этот топик меняется в зависимости от жизни сервера и рекомендаций, на какой сервер отправлять запросы.
         */
        private String topic;

        /**
         * Идентифкатор модуля сервера, которому направляются запросы.
         */
        private String serverNodeId;

        /**
         * Флаг того, что сервер сменился, нужно обновить топик.
         */
        private boolean serverChanged;

        private final Object sync = new Object();

        ClientInfo(Client client) {
            this.client = client;
            this.aliveNodePath = Paths.getClientPathOnNode(
                    NODE_ID,
                    client.getServerModuleId(),
                    client.getApiInterface().getName(),
                    client.getClientModuleId()
            );
        }

        String getTopic()
                throws InterruptedException {
            synchronized (sync) {
                if (serverNodeId == null || serverChanged) {
                    updateTopicIfNecessary();
                }

                if (serverNodeId == null) {
                    throw new NoRouteException();
                }

                return topic;
            }
        }

        void updateTopicIfNecessary()
                throws InterruptedException {
            synchronized (sync) {
                String currentServerNodeId = serverChanged ? null : serverNodeId;

                // Проверка жив ли сервер.
                if (currentServerNodeId != null &&
                        zkClient.isPathNotExists(getServerPathOnNode(currentServerNodeId))) {
                    // Сервер отключен.
                    currentServerNodeId = null;
                } else if (currentServerNodeId != null) {
                    // Сервер жив. Проверяем не сменился ли в рекомендуемых.
                    final String newServerNodeId = zkClient.read(aliveNodePath);

                    if (!Objects.equals(newServerNodeId, currentServerNodeId) &&
                            zkClient.isPathExists(getServerPathOnNode(newServerNodeId))) {
                        // Сервер сменился в рекомендуемых и он жив.
                        currentServerNodeId = newServerNodeId;
                    }
                }

                if (currentServerNodeId == null) {
                    // Нет указания на сервер, пытаемся получить рекомендуемый.
                    currentServerNodeId = zkClient.read(aliveNodePath);

                    if (zkClient.isPathNotExists(getServerPathOnNode(currentServerNodeId))) {
                        // Сервер из рекомендуемых отключен.
                        currentServerNodeId = null;
                    } else {
                        LOG.info("Recommended server selected, server: {}",
                                currentServerNodeId
                        );
                    }
                }

                if (currentServerNodeId == null) {
                    // Сервер не указан как рекомендуемый, ищем в списке доступных серверов.
                    final List<String> serverNodeIds = Paths.getNodesWithSpecifiedServer(
                            zkClient, client.getServerModuleId(), client.getApiInterface().getName()
                    );

                    if (!serverNodeIds.isEmpty()) {
                        // Выбираем случайный сервер.
                        currentServerNodeId = serverNodeIds.get(random.nextInt(serverNodeIds.size()));

                        LOG.info("Random server selected, server: {}",
                                currentServerNodeId
                        );
                    }
                }

                if (!Objects.equals(serverNodeId, currentServerNodeId)) {
                    // Пересоздаем имя топика на новый сервер.
                    updateServerNodeId(currentServerNodeId);
                }

                serverChanged = false;
            }
        }

        @Override
        public void process(WatchedEvent event) {
            synchronized (sync) {
                if ((
                        event.getType() == Event.EventType.NodeDataChanged &&
                                Objects.equals(event.getPath(), aliveNodePath)
                ) || (
                        event.getType() == Event.EventType.NodeDeleted &&
                                Objects.equals(event.getPath(), getServerPathOnNode(serverNodeId))
                )) {
                    // Изменился в рекомендуемых либо удален текущий, считаем в следующем запросе.
                    serverChanged = true;
                }
            }
        }

        private void updateServerNodeId(String newServerNodeId)
                throws InterruptedException {
            LOG.info("Client server changed, oldServer: {}, newServer: {}",
                    serverNodeId, newServerNodeId
            );

            if (serverNodeId != null) {
                final String oldPath = getServerPathOnNode(serverNodeId);
                try {
                    // Больше не следим за старым сервером.
                    if (!zkClient.removeWatcher(client, oldPath, this)) {
                        LOG.trace("No watcher, zkPath: {}", oldPath);
                    }
                } catch (Exception e) {
                    LOG.warn(MessageFormat.format(
                            "Failed to remove watcher, zkPath: {0}", oldPath
                    ), e);
                }
            }

            if (newServerNodeId != null) {
                // Следим за жизнью нового сервера.
                zkClient.addWatcher(client, getServerPathOnNode(newServerNodeId), this);
            }

            // Удаление узла в Zookeeper, которые отвечает за информацию, к какому серверу подключен данный клиент.
            zkClient.removeNode(
                    client,
                    Paths.getClientConnectionPathOnNode(
                            serverNodeId,
                            client.getServerModuleId(),
                            client.getApiInterface().getName(),
                            NODE_ID,
                            client.getClientModuleId()
                    )
            );

            // Новый узел в Zookeeper, которые отвечает за информацию, к какому серверу подключен данный клиент.
            zkClient.addEphemeralNode(
                    client,
                    Paths.getClientConnectionPathOnNode(
                            newServerNodeId,
                            client.getServerModuleId(),
                            client.getApiInterface().getName(),
                            NODE_ID,
                            client.getClientModuleId()
                    ),
                    null
            );

            serverNodeId = newServerNodeId;

            if (newServerNodeId != null) {
                // Имя топика сервера, куда будут направляться запросы.
                topic = MessageFormat.format(
                        "{0}-{1}-{2}",
                        serverNodeId,
                        client.getServerModuleId(),
                        client.getApiInterface().getName()
                );
            } else {
                // Нет живого сервера.
                topic = null;
            }
        }

        private String getServerPathOnNode(String nodeId) {
            return Paths.getServerPathOnNode(
                    nodeId, client.getServerModuleId(), client.getApiInterface().getName()
            );
        }

    }

    private static class ServerInfo {

        final Server server;

        /**
         * Эфемерный узел в Zookeeper, который существует пока жив сервер.
         */
        final String aliveNodePath;

        ServerInfo(Server server, Class<?> apiInterface) {
            this.server = server;

            this.aliveNodePath = Paths.getServerPathOnNode(
                    NODE_ID, server.getServerModuleId(), apiInterface.getName()
            );
        }

    }

}
