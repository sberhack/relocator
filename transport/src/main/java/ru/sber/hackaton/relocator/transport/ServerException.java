package ru.sber.hackaton.relocator.transport;

/**
 * Ошибка на сервере.
 */
public class ServerException extends RuntimeException {

    @SuppressWarnings("unused")
    public ServerException(String message) {
        super(message);
    }

    @SuppressWarnings("unused")
    public ServerException(String message, Throwable cause) {
        super(message, cause);
    }

    @SuppressWarnings("unused")
    public ServerException(Throwable cause) {
        super(cause);
    }

}
